static char help[] = "\n";
#include "pflotran.h"
#include "time.h"

#undef __FUNCT__
#define __FUNCT__ "PrecipitationCreateFromFile"
PetscErrorCode PrecipitationCreateFromFile(const char filename[],Precipitation *P)
{
  PetscFunctionBegin;
  PetscErrorCode ierr;
  PetscReal      t,p,T;
  PetscInt       N;
  FILE          *fp;

  // open file and get number of entries
  fp = fopen(filename,"r");
  if(!fp){ SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_FILE_OPEN,filename);}
  fscanf(fp,"%d\n",&N);

  // dimension space
  ierr = PetscMalloc3(N*sizeof(PetscReal),&(P->time),
		      N*sizeof(PetscReal),&(P->rain),
		      N*sizeof(PetscReal),&(P->temp));CHKERRQ(ierr);

  // read in contents
  P->N    = 0;
  P->prev = 0;
  while (fscanf(fp,"%lf %lf %lf\n",&t,&p,&T) == 3) {
    P->time[P->N] = t;
    P->rain[P->N] = p;
    P->temp[P->N] = T;
    P->N += 1;
  }
  if(P->N != N){ SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_ARG_OUTOFRANGE,"Inconsistent precipitation file"); }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "PrecipitationCreate"
PetscErrorCode PrecipitationCreate(Precipitation *P,PetscReal Tatm)
{
  PetscFunctionBegin;
  PetscErrorCode ierr;
  ierr = PetscMalloc3(2*sizeof(PetscReal),&(P->time),
		      2*sizeof(PetscReal),&(P->rain),
		      2*sizeof(PetscReal),&(P->temp));CHKERRQ(ierr);
  P->N    = 2;
  P->prev = 0;
  P->time[0] = 0; P->rain[0] = 0; P->temp[0] = Tatm;
  P->time[1] = 1; P->rain[1] = 0; P->temp[1] = Tatm;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "PrecipitationInterpolate"
PetscReal PrecipitationInterpolate(Precipitation *P,PetscReal t,SurfaceState *surface,
				   PetscReal *T,PetscReal *mass_rate_per_area,
				   PetscReal *energy_rate_per_area)
{
  PetscFunctionBegin;
  PetscReal pr=0,eta,etal,etai,U,Ul,Ui,xi,fct;
  PetscInt  i;

  // Interpolate the precipitation rate and atmosphere temperature
  if(t < P->time[0]){
    pr   = P->rain[0];
    (*T) = P->temp[0];
  }else if(t > P->time[P->N-1]){
    pr   = P->rain[P->N-1];
    (*T) = P->temp[P->N-1];
  }else{
    if(t < P->time[P->prev]) P->prev = 0; // fail-safe
    for(i=P->prev;i<P->N-1;i++){
      if(t >= P->time[i] && t <= P->time[i+1]){
	fct     = (t-P->time[i])/(P->time[i+1]-P->time[i]);
	P->prev = i;
	pr      = (1-fct)*P->rain[i] + fct*P->rain[i+1];
	(*T)    = (1-fct)*P->temp[i] + fct*P->temp[i+1];
	break;
      }
    }
  }

  // Evaluate state
  FrozenFraction_Conic((*T),&xi,NULL,surface->Tmin,surface->Tmax);
  WaterDensity((*T),PREF,&etal,NULL,NULL);
  IceDensity  ((*T),PREF,&etai,NULL,NULL);
  WaterInternalEnergy((*T),&Ul,NULL);
  IceInternalEnergy  ((*T),&Ui,NULL);
  eta = (1-xi)*etal+xi*etai;
  U   = (1-xi)*Ul  +xi*Ui;
  (*mass_rate_per_area)   = pr*eta;
  (*energy_rate_per_area) = (*mass_rate_per_area)*U;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "PrecipitationDestroy"
PetscErrorCode PrecipitationDestroy(Precipitation *P)
{
  PetscFunctionBegin;
  PetscErrorCode ierr;
  ierr = PetscFree3(P->time,P->rain,P->temp);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "Residual"
PetscErrorCode Residual(TS ts,PetscReal t,Vec U,Vec U_t,Vec R,void *ctx)
{
  PetscFunctionBegin;
  PetscErrorCode ierr;
  AppCtx        *user    = (AppCtx *)ctx;
  SurfaceState  *surface = &(user->surface);
  MeshInfo      *info    = &(user->info);
  Precipitation *prcp    = &(user->pr);
  DM             dm;
  ierr = TSGetDM(ts,&dm);CHKERRQ(ierr);

  // Load local arrays
  Vec          Ul, Ul_t;
  PetscScalar *ul,*ul_t,*r,*f,*s,*s_t,pnt2pnt[3],dist,dH,gT,MassFlux,EnergyFlux,ManningVel,agH,reg,u;
  ierr = DMGetLocalVector(dm,&Ul  );CHKERRQ(ierr);
  ierr = DMGetLocalVector(dm,&Ul_t);CHKERRQ(ierr);
  ierr = DMGlobalToLocalBegin(dm,U  ,INSERT_VALUES,Ul  );CHKERRQ(ierr);
  ierr = DMGlobalToLocalEnd  (dm,U  ,INSERT_VALUES,Ul  );CHKERRQ(ierr);
  ierr = DMGlobalToLocalBegin(dm,U_t,INSERT_VALUES,Ul_t);CHKERRQ(ierr);
  ierr = DMGlobalToLocalEnd  (dm,U_t,INSERT_VALUES,Ul_t);CHKERRQ(ierr);
  ierr = VecGetArray(Ul  ,&ul  );CHKERRQ(ierr);
  ierr = VecGetArray(Ul_t,&ul_t);CHKERRQ(ierr);
  ierr = VecGetArray(R   ,&r);CHKERRQ(ierr);

  // What are my 0- and 1-height point ranges?
  PetscInt p,pStart0,pEnd0,pStart1,pEnd1,dim=user->dim,ss,up,dn;
  ierr = DMPlexGetHeightStratum(dm,0,&pStart0,&pEnd0);CHKERRQ(ierr);
  ierr = DMPlexGetHeightStratum(dm,1,&pStart1,&pEnd1);CHKERRQ(ierr);

  // Evaluate the state
  PetscReal Tatm,mass_rate_per_area,energy_rate_per_area;
  ierr = SurfaceStateEvaluate(dm,ul,surface,NULL);CHKERRQ(ierr);
  ierr = PrecipitationInterpolate(prcp,t,surface,&Tatm,&mass_rate_per_area,&energy_rate_per_area);

  // Evaluate the residual, first loop over cells...
  PetscReal hl_t,hi_t;
  for(p=pStart0;p<pEnd0;p++){
    ierr = DMPlexPointGlobalRef(dm,p,r,&f);CHKERRQ(ierr);
    if(f){
      ierr = DMPlexPointGlobalRef(dm,p,ul  ,&s  );CHKERRQ(ierr); // s   = {h  ,T}
      ierr = DMPlexPointGlobalRef(dm,p,ul_t,&s_t);CHKERRQ(ierr); // s_t = {h_t,T_t}

      // time derivatives of liquid and ice heights
      hl_t = -surface->xi_T[p]*s_t[1]*s[0]+(1-surface->xi[p])*s_t[0];
      hi_t =  surface->xi_T[p]*s_t[1]*s[0]+(  surface->xi[p])*s_t[0];

      // Mass, d/dt(eta_l*h_l + eta_i*h_i) + ...
      f[0]  = surface->etal_T[p]*s_t[1]*surface->hl[p]; // liquid
      f[0] += surface->etal  [p]*hl_t;
      f[0] += surface->etai_T[p]*s_t[1]*surface->hi[p]; // ice
      f[0] += surface->etai  [p]*hi_t;

      // Energy, d/dt(eta_l*U_l*h_l + eta_i*U_i*h_i) + ...
      f[1]  = surface->etal_T[p]*s_t[1]*surface->hl[p]*surface->Ul[p]; // liquid
      f[1] += surface->etal  [p]*hl_t  *surface->Ul[p];
      f[1] += surface->etal  [p]*surface->hl[p]*surface->Ul_T[p]*s_t[1];
      f[1] += surface->etai_T[p]*s_t[1]*surface->hi[p]*surface->Ui[p]; // ice
      f[1] += surface->etai  [p]*hi_t  *surface->Ui[p];
      f[1] += surface->etai  [p]*surface->hi[p]*surface->Ui_T[p]*s_t[1];

      // Souces/sinks
      f[0] -= mass_rate_per_area;         // added mass from precipitation
      f[1] -= energy_rate_per_area;       // added energy from precipitation
      f[1] -= surface->hconv*(Tatm-s[1]); // added energy from convection to atmosphere
    }
  }

  // ...next over interfaces
  for(p=pStart1;p<pEnd1;p++){

    // Get the support of interface
    const PetscInt *supp;
    ierr = DMPlexGetSupportSize(dm,p,&ss  );CHKERRQ(ierr);
    ierr = DMPlexGetSupport    (dm,p,&supp);CHKERRQ(ierr);

    // Handle boundary conditions
    if(ss==1) continue; // Zero-Neumann

    // Estimate Manning's velocity
    Waxpy(dim,-1,&(info->X0[supp[0]*dim]),&(info->X0[supp[1]*dim]),pnt2pnt);dist=Norm(dim,pnt2pnt);
    if(surface->H[supp[0]]>surface->H[supp[1]]){
      up = supp[0]; dn = supp[1];
    }else{
      up = supp[1]; dn = supp[0];
    }
    dH  = surface->H[supp[1]]-surface->H[supp[0]];
    gT  = surface->T[supp[1]]-surface->T[supp[0]];
    agH = PetscAbsReal(dH/dist);
    if(agH > user->delta){
      reg = PetscSqrtReal(agH);
    }else{
      u   = agH/user->delta;
      reg = PetscSqrtReal(user->delta)*(2.5*(1.0-u)*u*u+u*u*u);
    }
    ManningVel = -PetscPowReal(PetscMax(0,surface->hl[up]),2./3.)/surface->Cf*reg*PetscSign(dH);

    // Compute mass and energy fluxes
    PetscReal avg_etal,avg_hl,avg_Ul,avg_xi;
    avg_etal    = 0.5*(surface->etal[up]+surface->etal[dn]);
    avg_hl      = 0.5*(surface->hl[up]  +surface->hl[dn]  );
    avg_Ul      = 0.5*(surface->Ul[up]  +surface->Ul[dn]  );
    avg_xi      = 0.5*(surface->xi[up]  +surface->xi[dn]  );
    MassFlux    = ManningVel*avg_hl*avg_etal;
    EnergyFlux  = MassFlux*avg_Ul;
    EnergyFlux -= ((1-avg_xi)*surface->kappal + avg_xi*surface->kappai)*gT/dist;
    
    // Load into the residual
    ierr = DMPlexPointGlobalRef(dm,supp[0],r,&f);CHKERRQ(ierr);
    if(f){
      f[0] += MassFlux  *info->V1[p-pStart1]/info->V0[supp[0]];
      f[1] += EnergyFlux*info->V1[p-pStart1]/info->V0[supp[0]];
    }
    ierr = DMPlexPointGlobalRef(dm,supp[1],r,&f);CHKERRQ(ierr);
    if(f){
      f[0] -= MassFlux  *info->V1[p-pStart1]/info->V0[supp[1]];
      f[1] -= EnergyFlux*info->V1[p-pStart1]/info->V0[supp[1]];
    }
  }

  // Cleanup
  ierr = VecRestoreArray(R   ,&r    );CHKERRQ(ierr);
  ierr = VecRestoreArray(Ul  ,&ul    );CHKERRQ(ierr);
  ierr = VecRestoreArray(Ul_t,&ul_t  );CHKERRQ(ierr);
  ierr = DMRestoreLocalVector(dm,&Ul  );CHKERRQ(ierr);
  ierr = DMRestoreLocalVector(dm,&Ul_t);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "MonitorMassEnergy"
PetscErrorCode MonitorMassEnergy(TS ts,PetscInt step,PetscReal time,Vec U,void *ctx)
{
  PetscFunctionBegin;
  DM                 dm;
  AppCtx            *user    = (AppCtx *)ctx;
  SurfaceState      *surface = &(user->surface);
  MeshInfo          *info    = &(user->info);
  Precipitation     *prcp    = &(user->pr);
  PetscInt           p,pStart,pEnd;
  const PetscScalar *constUarray;
  PetscScalar       *u,dt,mass_per,energy_per,Tatm;
  PetscScalar        mass_rate_per_area   = 0,energy_rate_per_area = 0;
  PetscScalar        local_surface_mass   = 0,surface_mass   = 0;
  PetscScalar        local_surface_energy = 0,surface_energy = 0;
  PetscScalar        local_precip_mass    = 0,precip_mass    = 0;
  PetscScalar        local_precip_energy  = 0,precip_energy  = 0;
  PetscScalar        local_atm_energy     = 0,atm_energy     = 0;
  PetscErrorCode     ierr;
  ierr = TSGetDM(ts,&dm);CHKERRQ(ierr);
  ierr = TSGetTimeStep(ts,&dt);CHKERRQ(ierr);
  ierr = VecGetArrayRead(U,&constUarray);CHKERRQ(ierr);
  PetscScalar *Uarray = (PetscScalar *)constUarray;
  ierr = SurfaceStateEvaluate(dm,Uarray,surface,user);CHKERRQ(ierr);
  ierr = PrecipitationInterpolate(prcp,time,surface,&Tatm,&mass_rate_per_area,&energy_rate_per_area);CHKERRQ(ierr);
  ierr = DMPlexGetHeightStratum(dm,0,&pStart,&pEnd);CHKERRQ(ierr);
  for(p=pStart;p<pEnd;p++){
    ierr = DMPlexPointGlobalRef(dm,p,Uarray,&u);CHKERRQ(ierr);
    if(u){
      local_surface_mass   += (surface->etal[p]*surface->hl[p]+
			       surface->etai[p]*surface->hi[p])*info->V0[p];
      local_surface_energy += (surface->etal[p]*surface->Ul[p]*surface->hl[p]+
			       surface->etai[p]*surface->Ui[p]*surface->hi[p])*info->V0[p];
      local_precip_mass    +=  mass_rate_per_area  *info->V0[p]*dt;
      local_precip_energy  +=  energy_rate_per_area*info->V0[p]*dt;
      local_atm_energy     +=  surface->hconv*(Tatm-u[1])*info->V0[p]*dt;
    }
  }
  ierr = VecRestoreArrayRead(U,&constUarray);CHKERRQ(ierr);
  ierr = MPI_Reduce((void *)(&local_surface_mass),
		    (void *)(&surface_mass),
		    1,MPI_DOUBLE,MPI_SUM,0,PETSC_COMM_WORLD);CHKERRQ(ierr);
  ierr = MPI_Reduce((void *)(&local_surface_energy),
		    (void *)(&surface_energy),
		    1,MPI_DOUBLE,MPI_SUM,0,PETSC_COMM_WORLD);CHKERRQ(ierr);
  ierr = MPI_Reduce((void *)(&local_precip_mass),
		    (void *)(&precip_mass),
		    1,MPI_DOUBLE,MPI_SUM,0,PETSC_COMM_WORLD);CHKERRQ(ierr);
  ierr = MPI_Reduce((void *)(&local_precip_energy),
		    (void *)(&precip_energy),
		    1,MPI_DOUBLE,MPI_SUM,0,PETSC_COMM_WORLD);CHKERRQ(ierr);
  ierr = MPI_Reduce((void *)(&local_atm_energy),
		    (void *)(&atm_energy),
		    1,MPI_DOUBLE,MPI_SUM,0,PETSC_COMM_WORLD);CHKERRQ(ierr);
  if(step==0){
    user->initial_mass   = surface_mass;
    user->initial_energy = surface_energy;
  }
  user->total_precip_mass   += precip_mass;
  user->total_precip_energy += precip_energy + atm_energy;
  mass_per = (surface_mass-user->initial_mass-user->total_precip_mass)*100.0;
  if(surface_mass > 1e-14) mass_per /= surface_mass;
  energy_per = (surface_energy-user->initial_energy-user->total_precip_energy)*100.0;
  if(surface_energy > 1e-14) energy_per /= surface_energy;

  ierr = PetscPrintf(PETSC_COMM_WORLD,"%d TS dt %.3e time %e mass %+.3f%% energy %+.3f%%\n",
		     step,dt,time,mass_per,energy_per);CHKERRQ(ierr);

  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "MonitorSingleCell"
PetscErrorCode MonitorSingleCell(TS ts,PetscInt step,PetscReal time,Vec U,void *ctx)
{
  PetscFunctionBegin;
  DM                 dm;
  const PetscScalar *constUarray;
  PetscErrorCode     ierr;
  ierr = TSGetDM(ts,&dm);CHKERRQ(ierr);
  ierr = VecGetArrayRead(U,&constUarray);CHKERRQ(ierr);
  PetscScalar *Uarray = (PetscScalar *)constUarray;
  ierr = VecRestoreArrayRead(U,&constUarray);CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_WORLD,"h %f T %f  ",Uarray[0],Uarray[1]);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "MonitorTwoCell"
PetscErrorCode MonitorTwoCell(TS ts,PetscInt step,PetscReal time,Vec U,void *ctx)
{
  PetscFunctionBegin;
  DM                 dm;
  const PetscScalar *constUarray;
  PetscErrorCode     ierr;
  ierr = TSGetDM(ts,&dm);CHKERRQ(ierr);
  ierr = VecGetArrayRead(U,&constUarray);CHKERRQ(ierr);
  PetscScalar *Uarray = (PetscScalar *)constUarray;
  ierr = VecRestoreArrayRead(U,&constUarray);CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_WORLD,"h %f %f T %f %f  ",Uarray[0],Uarray[2],Uarray[1],Uarray[3]);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "Output"
PetscErrorCode Output(TS ts,PetscInt step,PetscReal time,Vec U,void *ctx)
{
  PetscFunctionBegin;
  PetscErrorCode ierr;
  PetscInt       tstep;
  AppCtx        *user    = (AppCtx *)ctx;
  char filenametemplate[PETSC_MAX_PATH_LEN] = "surf%04d.vtu";
  if(time-user->prev_plot_t > user->plot_dt){
    tstep = (PetscInt)(time/user->plot_dt);
    ierr  = TSMonitorSolutionVTK(ts,tstep,time,U,&filenametemplate);CHKERRQ(ierr);
    user->prev_plot_t = time;
  }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "InitialCondition"
/*

 */
PetscErrorCode InitialCondition(DM dm,Vec U,PetscReal h0,PetscReal dh,PetscReal T0,PetscReal dT)
{
  PetscFunctionBegin;
  PetscErrorCode ierr;
  PetscScalar   *u,*s;
  PetscInt       p,pStart,pEnd;
  srand(time(NULL));
  ierr = VecGetArray(U,&u);CHKERRQ(ierr);
  ierr = DMPlexGetHeightStratum(dm,0,&pStart,&pEnd);CHKERRQ(ierr);
  for(p=pStart;p<pEnd;p++){
    ierr = DMPlexPointGlobalRef(dm,p,u,&s);CHKERRQ(ierr);
    if(s){
      s[0] = h0*(1+dh*(rand()/((PetscReal)RAND_MAX)-0.5));
      s[1] = T0*(1+dT*(rand()/((PetscReal)RAND_MAX)-0.5));
    }
  }
  ierr = VecRestoreArray(U,&u);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "main"
int main(int argc, char **argv)
{
  // Initialize
  MPI_Comm          comm;
  PetscErrorCode    ierr;
  PetscMPIInt       rank;
  ierr = PetscInitialize(&argc,&argv,(char*)0,help);CHKERRQ(ierr);
  comm = PETSC_COMM_WORLD;
  ierr = MPI_Comm_rank(comm,&rank);CHKERRQ(ierr);

  // Set some application context
  AppCtx user;
  user.delta          =  1.0;     // [-], regularization parameter
  user.plot_dt        =  86400.0; // [s], plotting frequency

  // Mass equation parameters
  user.surface.Cf     =  0.03;    // [s/(m^(1/3)], Manning's friction coefficient

  // Energy equation parameters
  user.surface.Tmin   = -1.0;     // [C], minimum temperature at which liquid water can exist
  user.surface.Tmax   =  1.0;     // [C], maximum temperature at which ice can exist
  user.surface.kappal =  0.6e-3;  // [kJ/(s m K)], thermal conductivity of liquid water
  user.surface.kappai =  2.1e-3;  // [kJ/(s m K)], thermal conductivity of ice
  user.surface.hconv  =  0.1;     // [kJ/(s m^2 K)], convection coefficient to air

  // Options
  char esrifile[PETSC_MAX_PATH_LEN] = "", prcpfile[PETSC_MAX_PATH_LEN] = "";
  PetscBool use_esri=PETSC_FALSE,use_prcp=PETSC_FALSE;
  PetscReal h0=0.1,dh=0.1,T0=10.0,dT=0.1;
  ierr = PetscOptionsBegin(comm,NULL,"Options","");CHKERRQ(ierr);
  ierr = PetscOptionsString("-esri","Name of esri file to use for elevation data"    ,"",esrifile,esrifile,sizeof(esrifile),&use_esri);CHKERRQ(ierr);
  ierr = PetscOptionsString("-pr"  ,"Name of text file to use for precipitation data","",prcpfile,prcpfile,sizeof(prcpfile),&use_prcp);CHKERRQ(ierr);
  ierr = PetscOptionsReal("-h0","Initial water height, h0 +- dh/2","",h0,&h0,NULL);CHKERRQ(ierr);
  ierr = PetscOptionsReal("-dh","Initial water height variation, h0 +- dh/2","",dh,&dh,NULL);CHKERRQ(ierr);
  ierr = PetscOptionsReal("-T0","Initial temperature, T0 +- dT/2","",T0,&T0,NULL);CHKERRQ(ierr);
  ierr = PetscOptionsReal("-dT","Initial temperature variation, T0 +- dT/2","",dT,&dT,NULL);CHKERRQ(ierr);
  ierr = PetscOptionsEnd();CHKERRQ(ierr);
  
  // Precipitation
  if(use_prcp){
    ierr = PrecipitationCreateFromFile(prcpfile,&(user.pr));CHKERRQ(ierr);
  }else{
    ierr = PrecipitationCreate(&(user.pr),T0);CHKERRQ(ierr);
  }

  // Finalize application context
  user.prev_plot_t         = -user.plot_dt;
  user.total_precip_mass   =  0.0;
  user.total_precip_energy =  0.0;

  // Create the mesh
  DM        dm,dmDist;
  PetscInt  overlap=1;
  const PetscReal lower[2] = {0,0};
  const PetscReal upper[2] = {1,1};
  const PetscInt  edges[2] = {1,1};
  ierr = DMPlexCreate(comm,&dm);CHKERRQ(ierr);
  ierr = DMSetDimension(dm,2);CHKERRQ(ierr);
  ierr = DMPlexCreateSquareMesh(dm,lower,upper,edges,DM_BOUNDARY_NONE,DM_BOUNDARY_NONE);CHKERRQ(ierr);
  ierr = DMViewFromOptions(dm,NULL,"-dm_view");CHKERRQ(ierr);

  // Tell the DM how degrees of freedom interact
  ierr = DMPlexSetAdjacencyUseCone   (dm,PETSC_TRUE );CHKERRQ(ierr);
  ierr = DMPlexSetAdjacencyUseClosure(dm,PETSC_FALSE);CHKERRQ(ierr);

  // Distribute the mesh
  ierr = DMPlexDistribute(dm,overlap,NULL,&dmDist);CHKERRQ(ierr);
  if (dmDist) { ierr = DMDestroy(&dm);CHKERRQ(ierr); dm = dmDist; }
  ierr = DMGetDimension(dm,&(user.dim));CHKERRQ(ierr);

  // Setup the section
  PetscSection sec;
  PetscInt     p,pStart,pEnd;
  ierr = PetscSectionCreate(PetscObjectComm((PetscObject)dm),&sec);CHKERRQ(ierr);
  ierr = PetscSectionSetNumFields(sec,2);CHKERRQ(ierr);
  ierr = DMPlexGetChart(dm,&pStart,&pEnd);CHKERRQ(ierr);
  ierr = PetscSectionSetChart(sec,pStart,pEnd);CHKERRQ(ierr);
  ierr = PetscSectionSetFieldName      (sec,0,"Depth");CHKERRQ(ierr);
  ierr = PetscSectionSetFieldComponents(sec,0,1);CHKERRQ(ierr);
  ierr = PetscSectionSetFieldName      (sec,1,"Temperature");CHKERRQ(ierr);
  ierr = PetscSectionSetFieldComponents(sec,1,1);CHKERRQ(ierr);

  // Set two dofs on all 0-height cells
  ierr = DMPlexGetHeightStratum(dm,0,&pStart,&pEnd);CHKERRQ(ierr);
  for(p=pStart;p<pEnd;p++){
    ierr = PetscSectionSetFieldDof(sec,p,0,1);CHKERRQ(ierr);
    ierr = PetscSectionSetFieldDof(sec,p,1,1);CHKERRQ(ierr);
    ierr = PetscSectionSetDof     (sec,p  ,2);CHKERRQ(ierr);
  }
  ierr = PetscSectionSetUp(sec);CHKERRQ(ierr);
  ierr = DMSetDefaultSection(dm,sec);CHKERRQ(ierr);
  ierr = PetscSectionDestroy(&sec);CHKERRQ(ierr);

  // Setup problem info
  ierr = MeshInfoCreate(dm,&(user.info));CHKERRQ(ierr);
  ierr = SurfaceStateCreate(dm,&(user.surface));CHKERRQ(ierr);
  if(use_esri){
    ierr = SurfaceStateElevation(&(user.surface),&(user.info),esrifile);CHKERRQ(ierr);
  }

  // Create a vec for the initial condition (constant pressure)
  Vec U;
  ierr = DMCreateGlobalVector(dm,&U);CHKERRQ(ierr);
  ierr = InitialCondition(dm,U,h0,dh,T0,dT);CHKERRQ(ierr);
  ierr = PetscObjectSetName((PetscObject)U,"SurfaceWater");CHKERRQ(ierr);

  // Create time stepping and solve
  TS ts;
  ierr = TSCreate(comm,&ts);CHKERRQ(ierr);
  ierr = TSSetType(ts,TSBEULER);CHKERRQ(ierr);
  ierr = TSSetIFunction(ts,NULL,Residual,&user);CHKERRQ(ierr);
  ierr = TSSetDM(ts,dm);CHKERRQ(ierr);
  ierr = TSSetSolution(ts,U);CHKERRQ(ierr);
  if(edges[0]*edges[1] == 1){
    ierr = TSMonitorSet(ts,MonitorSingleCell,&user,NULL);CHKERRQ(ierr);
  }else if(edges[0]*edges[1] == 2){
    ierr = TSMonitorSet(ts,MonitorTwoCell,&user,NULL);CHKERRQ(ierr);
  }
  ierr = TSMonitorSet(ts,MonitorMassEnergy,&user,NULL);CHKERRQ(ierr);
  ierr = TSMonitorSet(ts,Output,&user,NULL);CHKERRQ(ierr);
  ierr = TSSetDuration(ts,PETSC_MAX_INT,user.pr.time[user.pr.N-1]);CHKERRQ(ierr);
  ierr = TSSetFromOptions(ts);CHKERRQ(ierr);
  ierr = TSSetUp(ts);CHKERRQ(ierr);
  ierr = TSSolve(ts,U);CHKERRQ(ierr);

  // Cleanup
  ierr = PrecipitationDestroy(&(user.pr));CHKERRQ(ierr);
  ierr = MeshInfoDestroy(&(user.info));CHKERRQ(ierr);
  ierr = SurfaceStateDestroy(&(user.surface));CHKERRQ(ierr);
  ierr = TSDestroy(&ts);CHKERRQ(ierr);
  ierr = VecDestroy(&U);CHKERRQ(ierr);
  ierr = DMDestroy(&dm);CHKERRQ(ierr);
  ierr = PetscFinalize();CHKERRQ(ierr);
  return(0);
}
