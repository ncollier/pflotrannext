#include "pflotran.h"

#undef __FUNCT__
#define __FUNCT__ "SurfaceStateCreate"
PetscErrorCode SurfaceStateCreate(DM dm,SurfaceState *state)
{
  PetscFunctionBegin;
  PetscErrorCode ierr;
  PetscInt pStart,pEnd;
  ierr = DMPlexGetHeightStratum(dm,0,&pStart,&pEnd);CHKERRQ(ierr);
  state->n = pEnd-pStart;
  if(state->n == 0) PetscFunctionReturn(0);
  ierr = PetscMalloc4(state->n*sizeof(PetscReal),&(state->etal),
		      state->n*sizeof(PetscReal),&(state->etal_T),
                      state->n*sizeof(PetscReal),&(state->etai),
		      state->n*sizeof(PetscReal),&(state->etai_T));CHKERRQ(ierr);
  ierr = PetscMalloc4(state->n*sizeof(PetscReal),&(state->Ul),
		      state->n*sizeof(PetscReal),&(state->Ul_T),
                      state->n*sizeof(PetscReal),&(state->Ui),
		      state->n*sizeof(PetscReal),&(state->Ui_T));CHKERRQ(ierr);
  ierr = PetscMalloc2(state->n*sizeof(PetscReal),&(state->hl),
		      state->n*sizeof(PetscReal),&(state->hi));CHKERRQ(ierr);
  ierr = PetscMalloc2(state->n*sizeof(PetscReal),&(state->xi),
		      state->n*sizeof(PetscReal),&(state->xi_T));CHKERRQ(ierr);
  ierr = PetscMalloc2(state->n*sizeof(PetscReal),&(state->H),
		      state->n*sizeof(PetscReal),&(state->T));CHKERRQ(ierr);
  ierr = PetscCalloc1(state->n*sizeof(PetscReal),&(state->Z));CHKERRQ(ierr); // Z is initially zeroed out
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "SurfaceStateDestroy"
PetscErrorCode SurfaceStateDestroy(SurfaceState *state)
{
  PetscFunctionBegin;
  PetscErrorCode ierr;
  if(state->n == 0) PetscFunctionReturn(0);
  ierr = PetscFree4(state->etal,state->etal_T,state->etai,state->etai_T);CHKERRQ(ierr);
  ierr = PetscFree4(state->Ul  ,state->Ul_T  ,state->Ui  ,state->Ui_T  );CHKERRQ(ierr);
  ierr = PetscFree2(state->hl  ,state->hi);CHKERRQ(ierr);
  ierr = PetscFree2(state->xi  ,state->xi_T);CHKERRQ(ierr);
  ierr = PetscFree3(state->H   ,state->Z,state->T);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "SurfaceStateEvaluate"
PetscErrorCode SurfaceStateEvaluate(DM dm,PetscScalar *U,SurfaceState *state,AppCtx *user)
{
  PetscFunctionBegin;
  PetscInt  i;
  PetscReal h,T;
  if (state->n == 0) PetscFunctionReturn(0);
  for(i=0;i<state->n;i++){
    h  = U[2*i  ];
    T  = U[2*i+1];
    state->T[i]  = T;
    state->H[i]  = h+state->Z[i];
    FrozenFraction_Conic(T,&(state->xi[i]),&(state->xi_T[i]),state->Tmin,state->Tmax);
    state->hl[i] = h*(1-state->xi[i]);
    state->hi[i] = h*   state->xi[i];
    WaterDensity(T,PREF,&(state->etal[i]),&(state->etal_T[i]),NULL);
    IceDensity  (T,PREF,&(state->etai[i]),&(state->etai_T[i]),NULL);
    WaterInternalEnergy(T,&(state->Ul[i]),&(state->Ul_T[i]));
    IceInternalEnergy  (T,&(state->Ui[i]),&(state->Ui_T[i]));
  }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "SurfaceStateElevation"
PetscErrorCode SurfaceStateElevation(SurfaceState *state,MeshInfo *info,const char filename[])
{
  PetscFunctionBegin;
  PetscErrorCode ierr;
  PetscInt  i,dim=2;
  ESRI      esri;
  ierr = ESRICreateFromFile(filename,&esri);CHKERRQ(ierr);
  for(i=0;i<state->n;i++){
    state->Z[i] = ESRIInterpolate(&esri,&(info->X0[i*dim]),PETSC_TRUE);
  }
  ierr = ESRIDelete(&esri);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}
