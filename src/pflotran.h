#ifndef _PFLOTRAN_H_
#define _PFLOTRAN_H_
#include <petsc.h>

/* Constants */

#define HEAT_OF_FUSION 3.34e5   // [J/kg]
#define FMWH2O         18.01534 // [kg/kmol]
#define TREF           273.15   // [K]
#define PREF           101325.  // [Pa]     

/* Data structures */

typedef struct {
  PetscInt nrows,ncols;
  PetscReal xllcorner,yllcorner,cellsize,NODATA_value,*data;
} ESRI;

typedef struct {
  PetscReal *X0,*V0;     // 0-height centroids and volumes
  PetscReal *X1,*V1,*N1; // 1-height centroids, volumes, and normals
  PetscReal *X2,*V2,*N2; // 2-height centroids, volumes, and normals  

  // 1- and 2-height index sets
  IS              top1IS, top2IS;
  const PetscInt *top1,  *top2; 
  PetscInt        n1,     n2;

} MeshInfo;

typedef struct {
  PetscReal  n,m,alpha,Sr,Ss,mu,Ki,Ks;
  PetscReal *Sl,   *Si,   *Sg;          // saturations
  PetscReal *Sl_Pl,*Si_Pl,*Sg_Pl;       // d/dPl
  PetscReal *Sl_T, *Si_T, *Sg_T ;       // d/dT
  PetscReal *etal,   *etai,   *etag;    // molar densities
  PetscReal *etal_Pl,*etai_Pl,*etag_Pl; // d/dPl
  PetscReal *etal_T, *etai_T, *etag_T ; // d/dT
  PetscReal *Ul,   *Ui,   *Ug;          // internal energy
  PetscReal *Ul_Pl,*Ui_Pl,*Ug_Pl;       // d/dPl
  PetscReal *Ul_T, *Ui_T, *Ug_T ;       // d/dT
  PetscReal *phi,*phi_Pl;               // porosity
  PetscReal *Kr;                        // relative permeability
  PetscReal *Psi;                       // total potential
} SoilState;

typedef struct {
  PetscInt   n;
  PetscReal  Cf,Tmin,Tmax,kappal,kappai,hconv;
  PetscReal *etal,   *etai;    // molar densities
  PetscReal *etal_T, *etai_T;  // d/dT
  PetscReal *Ul,     *Ui;      // internal energy
  PetscReal *Ul_T,   *Ui_T;    // d/dT
  PetscReal *hl,     *hi;      // heigths
  PetscReal *xi,     *xi_T;    // frozen fraction
  PetscReal *H;                // total head
  PetscReal *Z;                // elevation
  PetscReal *T;                // temperature
} SurfaceState;

typedef struct {
  PetscReal *time,*rain,*temp;
  PetscInt   prev,N;
} Precipitation;

typedef struct {
  PetscInt      dim,averaging;
  PetscReal     g[3],delta,dt_target,initial_mass,initial_energy;
  PetscReal     total_precip_mass,total_precip_energy;
  PetscReal     prev_plot_t,plot_dt; 
  SoilState     soil;
  SurfaceState  surface;
  MeshInfo      info;
  Precipitation pr;
} AppCtx;

/* ESRI */

PETSC_EXTERN PetscErrorCode ESRICreateFromFile(const char filename[],ESRI *esri);
PETSC_EXTERN PetscReal ESRIInterpolate(ESRI *esri,PetscReal *x,PetscBool shift);
PETSC_EXTERN PetscErrorCode ESRIDelete(ESRI *esri);

/* Mesh info */

typedef PetscBool (*TestPoint)(PetscReal v,PetscReal *x,PetscReal *n,PetscInt dim);

PETSC_EXTERN PetscErrorCode MarkTopBoundary(DM dm,TestPoint test);
PETSC_EXTERN PetscErrorCode MeshInfoCreate(DM dm,MeshInfo *info);
PETSC_EXTERN PetscErrorCode MeshInfoDestroy(MeshInfo *info);

/* Water equation of state */


PETSC_EXTERN PetscErrorCode IceDensity(PetscReal T,PetscReal P,PetscReal *eta,PetscReal *eta_T,PetscReal *eta_P);
PETSC_EXTERN PetscErrorCode IceInternalEnergy(PetscReal T,PetscReal *U,PetscReal *U_T);
PETSC_EXTERN PetscErrorCode WaterDensity(PetscReal T,PetscReal P,PetscReal *eta,PetscReal *eta_T,PetscReal *eta_P);
PETSC_EXTERN PetscErrorCode WaterInternalEnergy(PetscReal T,PetscReal *U,PetscReal *U_T);

PETSC_EXTERN PetscErrorCode IceDensity(PetscReal T,PetscReal P,PetscReal *eta,PetscReal *eta_T,PetscReal *eta_P);
PETSC_EXTERN PetscErrorCode IceInternalEnergy(PetscReal T,PetscReal *U,PetscReal *U_T);
PETSC_EXTERN PetscErrorCode WaterDensity(PetscReal T,PetscReal P,PetscReal *eta,PetscReal *eta_T,PetscReal *eta_P);
PETSC_EXTERN PetscErrorCode WaterInternalEnergy(PetscReal T,PetscReal *U,PetscReal *U_T);

/* Hydrology */

typedef PetscErrorCode (*PressureSaturationFunction)(PetscReal,PetscReal,PetscReal,PetscReal,PetscReal *,PetscReal *);

PETSC_EXTERN void PressureSaturation_VanGenuchten(PetscReal n,PetscReal m,PetscReal alpha,PetscReal Pc,PetscReal *Se,PetscReal *dSe_dPc);
PETSC_EXTERN void PressureSaturation_Gardner(PetscReal n,PetscReal m,PetscReal alpha,PetscReal Pc,PetscReal *Se,PetscReal *dSe_dPc);
PETSC_EXTERN void RelativePermeability_Mualem(PetscReal m,PetscReal Se,PetscReal *Kr,PetscReal *dKr_dSe);
PETSC_EXTERN void RelativePermeability_Irmay(PetscReal m,PetscReal Se,PetscReal *Kr,PetscReal *dKr_dSe);
PETSC_EXTERN void FrozenFraction_Conic(PetscReal T,PetscReal *xi,PetscReal *xi_T,PetscReal Tmin,PetscReal Tmax);

/* State evaluation */

PETSC_EXTERN PetscErrorCode SoilStateCreate(DM dm,SoilState *state);
PETSC_EXTERN PetscErrorCode SoilStateDestroy(SoilState *state);
PETSC_EXTERN PetscErrorCode SoilStateEvaluate(DM dm,PetscScalar *Pl,SoilState *state,AppCtx *user);
PETSC_EXTERN PetscErrorCode SurfaceStateCreate(DM dm,SurfaceState *state);
PETSC_EXTERN PetscErrorCode SurfaceStateDestroy(SurfaceState *state);
PETSC_EXTERN PetscErrorCode SurfaceStateEvaluate(DM dm,PetscScalar *U,SurfaceState *state,AppCtx *user);
PETSC_EXTERN PetscErrorCode SurfaceStatePrint(SurfaceState *state);
PETSC_EXTERN PetscErrorCode SurfaceStateElevation(SurfaceState *state,MeshInfo *info,const char filename[]);

/* Misc */

PETSC_STATIC_INLINE void Waxpy(PetscInt dim, PetscScalar a, const PetscScalar *x, const PetscScalar *y, PetscScalar *w) {PetscInt d; for (d = 0; d < dim; ++d) w[d] = a*x[d] + y[d];}
PETSC_STATIC_INLINE PetscScalar Dot(PetscInt dim, const PetscScalar *x, const PetscScalar *y) {PetscScalar sum = 0.0; PetscInt d; for (d = 0; d < dim; ++d) sum += x[d]*y[d]; return sum;}
PETSC_STATIC_INLINE PetscReal Norm(PetscInt dim, const PetscScalar *x) {return PetscSqrtReal(PetscAbsScalar(Dot(dim,x,x)));}

#endif
