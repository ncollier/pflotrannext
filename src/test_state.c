static char help[] = "\n";
#include "pflotran.h"

#undef __FUNCT__
#define __FUNCT__ "OnTheTop"
/* 
   Mesh-specific definition of what it means to be on the top of a mesh
*/
PetscBool OnTheTop(PetscReal volume,PetscReal *centroid,PetscReal *normal,PetscInt dim)
{
  if(fabs(centroid[dim-1]-0.5)<10*PETSC_MACHINE_EPSILON) return PETSC_TRUE;
  return PETSC_FALSE;
}

#undef __FUNCT__
#define __FUNCT__ "InitialCondition"
/*
  Set the liquid pressure to P0 and the surface water height to h0.
 */
PetscErrorCode InitialCondition(DM dm,Vec U,PetscReal P0,PetscReal h0)
{
  PetscFunctionBegin;
  PetscErrorCode ierr;
  PetscScalar   *u,*Pl,*h;
  PetscInt       p,pStart,pEnd;
  ierr = VecGetArray(U,&u);CHKERRQ(ierr);
  ierr = DMPlexGetHeightStratum(dm,0,&pStart,&pEnd);CHKERRQ(ierr);
  for(p=pStart;p<pEnd;p++){
    ierr = DMPlexPointGlobalRef(dm,p,u,&Pl);CHKERRQ(ierr);
    if(Pl) *Pl = P0;
  }
  ierr = DMPlexGetHeightStratum(dm,1,&pStart,&pEnd);CHKERRQ(ierr);
  for(p=pStart;p<pEnd;p++){
    ierr = DMPlexPointGlobalRef(dm,p,u,&h);CHKERRQ(ierr);
    if(h) *h = rand()/((PetscReal)RAND_MAX);
  }
  ierr = VecRestoreArray(U,&u);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "main"
int main(int argc, char **argv)
{

  // Initialize
  MPI_Comm          comm;
  PetscErrorCode    ierr;
  PetscMPIInt       rank;
  ierr = PetscInitialize(&argc,&argv,(char*)0,help);CHKERRQ(ierr);
  comm = PETSC_COMM_WORLD;
  ierr = MPI_Comm_rank(comm,&rank);CHKERRQ(ierr);

  // Options
  char filename[PETSC_MAX_PATH_LEN] = "../data/simple.e";
  ierr = PetscOptionsBegin(comm,NULL,"Options","");CHKERRQ(ierr);
  ierr = PetscOptionsString("-mesh","Exodus.II filename to read","",filename,filename,sizeof(filename),NULL);CHKERRQ(ierr);
  ierr = PetscOptionsEnd();CHKERRQ(ierr);

  // Create the mesh
  DM        dm;
  ierr = DMPlexCreateExodusFromFile(comm,filename,PETSC_TRUE,&dm);CHKERRQ(ierr);
  ierr = MarkTopBoundary(dm,OnTheTop);CHKERRQ(ierr);
  ierr = DMSetFromOptions(dm);CHKERRQ(ierr);

  SurfaceState state;
  ierr = SurfaceStateCreate(dm,&state);CHKERRQ(ierr);
  state.Tmin = -1;
  state.Tmax =  1;
  state.n    =  1;

  PetscScalar U[2] = {1.0,1.0};
  PetscScalar hmin =   0, hmax = 0.1, dh = (hmax-hmin)/100.;
  PetscScalar Tmin = -10, Tmax = 10,  dT = (Tmax-Tmin)/100.;
  PetscScalar h,T;
  PetscInt i,j;
  for(i=0;i<=100;i++){
    h = hmin + dh*i;
    for(j=0;j<=100;j++){
      T = Tmin + dT*j;
      U[0] = h; U[1] = T;
      ierr = SurfaceStateEvaluate(dm,U,&state,NULL);CHKERRQ(ierr);
      printf("%e %e %e %e %e %e %e %e %e %e %e %e %e %e\n",
	     h,
	     T,
	     state.etal[0],
	     state.etai[0],
	     state.etal_T[0],
	     state.etai_T[0], 
	     state.Ul[0],
	     state.Ui[0],
	     state.Ul_T[0],
	     state.Ui_T[0],
	     state.hl[0],
	     state.hi[0],
	     state.xi[0],
	     state.xi_T[0]);
    }
  }

  // Cleanup
  ierr = SurfaceStateDestroy(&state);CHKERRQ(ierr);
  ierr = DMDestroy(&dm);CHKERRQ(ierr);
  ierr = PetscFinalize();CHKERRQ(ierr);
  return(0);
}
