static char help[] = "Richards equation (RE) with constant density, coupled to diffusive wave approximation to shallow water equations (DSW)\n";
#include <petsc.h>
#include "hydrology.h"
#include "time.h"

PETSC_STATIC_INLINE void Waxpy(PetscInt dim, PetscScalar a, const PetscScalar *x, const PetscScalar *y, PetscScalar *w) {PetscInt d; for (d = 0; d < dim; ++d) w[d] = a*x[d] + y[d];}
PETSC_STATIC_INLINE PetscScalar Dot(PetscInt dim, const PetscScalar *x, const PetscScalar *y) {PetscScalar sum = 0.0; PetscInt d; for (d = 0; d < dim; ++d) sum += x[d]*y[d]; return sum;}
PETSC_STATIC_INLINE PetscReal Norm(PetscInt dim, const PetscScalar *x) {return PetscSqrtReal(PetscAbsScalar(Dot(dim,x,x)));}

typedef struct {
  PetscReal *X0,*V0;     // 0-height centroids and volumes
  PetscReal *X1,*V1,*N1; // 1-height centroids, volumes, and normals
  PetscReal *X2,*V2,*N2; // 2-height centroids, volumes, and normals  
} MeshInfo;

typedef struct {
  PetscReal  porosity,n,m,alpha,Sr,Ss,rho,mu,Ki,Ks;
  PetscReal *S;    // saturation
  PetscReal *S_Pl; // d/dPl(S)
  PetscReal *Kr;   // relative permeability
  PetscReal *Psi;  // total potential
} SoilState;

typedef struct {
  PetscInt   dim,averaging;
  PetscReal  Pr,g[3],mass;
  SoilState  state;
  MeshInfo   info;
} AppCtx;

#undef __FUNCT__
#define __FUNCT__ "Solution_Hydrostatic"
PetscReal Solution_Hydrostatic(PetscReal *x,AppCtx *user)
{
  return user->Pr+user->state.rho*Dot(user->dim,user->g,x);
}

#undef __FUNCT__
#define __FUNCT__ "MeshInfoCreate"
PetscErrorCode MeshInfoCreate(DM dm,MeshInfo *info)
{
  PetscFunctionBegin;
  PetscErrorCode ierr;
  PetscInt       dim,i,p,pStart,pEnd,nCells;
  PetscReal      dummy[3];
  ierr   = DMGetDimension(dm,&dim);CHKERRQ(ierr);
  ierr   = DMPlexGetHeightStratum(dm,0,&pStart,&pEnd);CHKERRQ(ierr);
  nCells = pEnd-pStart;
  ierr   = PetscMalloc2(nCells*dim*sizeof(PetscReal),&(info->X0),
			nCells    *sizeof(PetscReal),&(info->V0));CHKERRQ(ierr);
  for(p=pStart;p<pEnd;p++){
    i    = p-pStart;
    ierr = DMPlexComputeCellGeometryFVM(dm,p,&(info->V0[i]),&(info->X0[i*dim]),dummy);CHKERRQ(ierr);
  }
  ierr   = DMPlexGetHeightStratum(dm,1,&pStart,&pEnd);CHKERRQ(ierr);
  nCells = pEnd-pStart;
  ierr   = PetscMalloc3(nCells*dim*sizeof(PetscReal),&(info->X1),
			nCells*dim*sizeof(PetscReal),&(info->N1),
			nCells*    sizeof(PetscReal),&(info->V1));CHKERRQ(ierr);
  for(p=pStart;p<pEnd;p++){
    i    = p-pStart;
    ierr = DMPlexComputeCellGeometryFVM(dm,p,&(info->V1[i]),&(info->X1[i*dim]),&(info->N1[i*dim]));CHKERRQ(ierr);
  }
  ierr   = DMPlexGetHeightStratum(dm,2,&pStart,&pEnd);CHKERRQ(ierr);
  nCells = pEnd-pStart;
  ierr   = PetscMalloc3(nCells*dim*sizeof(PetscReal),&(info->X2),
			nCells*dim*sizeof(PetscReal),&(info->N2),
			nCells*    sizeof(PetscReal),&(info->V2));CHKERRQ(ierr);
  for(p=pStart;p<pEnd;p++){
    i    = p-pStart;
    if(dim==2){
      info->V2[i] = 1; 
    }else{
      ierr = DMPlexComputeCellGeometryFVM(dm,p,&(info->V2[i]),&(info->X2[i*dim]),&(info->N2[i*dim]));CHKERRQ(ierr);
    }
  }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "MeshInfoDestroy"
PetscErrorCode MeshInfoDestroy(MeshInfo *info)
{
  PetscFunctionBegin;
  PetscErrorCode ierr;
  ierr = PetscFree2(info->X0,info->V0);CHKERRQ(ierr);
  ierr = PetscFree3(info->X1,info->V1,info->N1);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "SoilStateCreate"
PetscErrorCode SoilStateCreate(DM dm,SoilState *state)
{
  PetscFunctionBegin;
  PetscErrorCode ierr;
  PetscInt pStart,pEnd,nCells;
  ierr   = DMPlexGetHeightStratum(dm,0,&pStart,&pEnd);CHKERRQ(ierr);
  nCells = pEnd-pStart;
  ierr   = PetscMalloc4(nCells*sizeof(PetscReal),&(state->S),
			nCells*sizeof(PetscReal),&(state->S_Pl),
			nCells*sizeof(PetscReal),&(state->Kr),
			nCells*sizeof(PetscReal),&(state->Psi));CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "SoilStateDestroy"
PetscErrorCode SoilStateDestroy(SoilState *state)
{
  PetscFunctionBegin;
  PetscErrorCode ierr;
  ierr = PetscFree4(state->S,state->S_Pl,state->Kr,state->Psi);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "SoilStateEvaluate"
PetscErrorCode SoilStateEvaluate(DM dm,PetscScalar *Pl,SoilState *state,AppCtx *user)
{
  PetscFunctionBegin;
  PetscErrorCode  ierr;
  PetscInt        dim,i,pStart,pEnd,nCells;
  PetscReal       Se,Se_Pc;
  MeshInfo       *info = &(user->info);
  ierr   = DMPlexGetHeightStratum(dm,0,&pStart,&pEnd);CHKERRQ(ierr);
  nCells = pEnd-pStart;
  dim    = user->dim;
  for(i=0;i<nCells;i++){
    PressureSaturation_VanGenuchten(state->n,state->m,state->alpha,user->Pr-Pl[i],&Se,&Se_Pc);
    RelativePermeability_Mualem(state->m,Se,&(state->Kr[i]),NULL);
    state->S[i]    =  (state->Ss-state->Sr)*Se+state->Sr;
    state->S_Pl[i] = -Se_Pc/(state->Ss-state->Sr);
    state->Psi[i]  =  Pl[i]-state->rho*Dot(dim,user->g,&(info->X0[i*dim]));
  }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "RichardsResidual"
PetscErrorCode RichardsResidual(TS ts,PetscReal t,Vec U,Vec U_t,Vec R,void *ctx)
{
  PetscFunctionBegin;
  AppCtx        *user  = (AppCtx *)ctx;
  SoilState     *state = &(user->state);
  MeshInfo      *info  = &(user->info);
  PetscErrorCode ierr;
  DM             dm;
  ierr = TSGetDM(ts,&dm);CHKERRQ(ierr);

  // Global --> Local of the solution and its time derivative
  Vec          Ul,Ul_t;
  PetscScalar *Pl,*Pl_t;
  ierr = DMGetLocalVector(dm,&Ul  );CHKERRQ(ierr);
  ierr = DMGetLocalVector(dm,&Ul_t);CHKERRQ(ierr);
  ierr = DMGlobalToLocalBegin(dm,U  ,INSERT_VALUES,Ul  );CHKERRQ(ierr);
  ierr = DMGlobalToLocalEnd  (dm,U  ,INSERT_VALUES,Ul  );CHKERRQ(ierr);
  ierr = DMGlobalToLocalBegin(dm,U_t,INSERT_VALUES,Ul_t);CHKERRQ(ierr); // don't need ghost values for U_t
  ierr = DMGlobalToLocalEnd  (dm,U_t,INSERT_VALUES,Ul_t);CHKERRQ(ierr);
  ierr = VecGetArray(Ul  ,&Pl  );CHKERRQ(ierr);
  ierr = VecGetArray(Ul_t,&Pl_t);CHKERRQ(ierr);

  // Richard's equation

  // Evaluate equations of state
  ierr = SoilStateEvaluate(dm,Pl,state,user);CHKERRQ(ierr);

  // Compute the residual
  PetscInt     pStart0,pEnd0,pStart1,pEnd1,pStart2,pEnd2;
  PetscInt     p,ss,dim = user->dim;
  PetscReal   *Rarray,*r,pnt2pnt[3],dist,DarcyFlux,Kr;
  ierr = VecGetArray(R,&Rarray);CHKERRQ(ierr);

  ierr = DMPlexGetHeightStratum(dm,0,&pStart0,&pEnd0);CHKERRQ(ierr);
  ierr = DMPlexGetHeightStratum(dm,1,&pStart1,&pEnd1);CHKERRQ(ierr);
  ierr = DMPlexGetHeightStratum(dm,2,&pStart2,&pEnd2);CHKERRQ(ierr);

  // Residual = d/dt( porosity * S ) ...
  for(p=pStart0;p<pEnd0;p++){
    ierr = DMPlexPointGlobalRef(dm,p,Rarray,&r);CHKERRQ(ierr);
    if(r) *r = state->porosity*state->S_Pl[p]*Pl_t[p];
  }

  // ... - sum( Darcy velocity * interface area / cell volume )
  for(p=pStart1;p<pEnd1;p++){

    // Get support of interface
    const PetscInt *supp;
    ierr = DMPlexGetSupportSize(dm,p,&ss  );CHKERRQ(ierr);
    ierr = DMPlexGetSupport    (dm,p,&supp);CHKERRQ(ierr);

    // Handle boundary conditions
    if(ss==1){
      continue; // 0 Neumann for now
      PetscReal *X0 = &(info->X0[    supp[0]*dim]); 
      PetscReal *X1 = &(info->X1[(p-pStart1)*dim]); 
      Waxpy(dim,-1,X0,X1,pnt2pnt); dist = Norm(dim,pnt2pnt);
      PetscReal  DirichletPl  = Solution_Hydrostatic(X1,user);
      PetscReal  DirichletPsi = DirichletPl - state->rho*Dot(dim,user->g,X1);
      DarcyFlux = -state->Ki*state->Kr[supp[0]]/state->mu*(DirichletPsi-state->Psi[supp[0]])/dist;
      ierr = DMPlexPointGlobalRef(dm,supp[0],Rarray,&r);CHKERRQ(ierr);
      if(r) *r += DarcyFlux*info->V1[p-pStart1]/info->V0[supp[0]];
      continue;
    }

    // Estimate the Darcy flux using a two point flux
    Waxpy(dim,-1,&(info->X0[supp[0]*dim]),&(info->X0[supp[1]*dim]),pnt2pnt); dist = Norm(dim,pnt2pnt);
    if(user->averaging == 0){  // upwind the relative permeability
      Kr = (state->Psi[supp[1]] > state->Psi[supp[0]]) ? state->Kr[supp[1]] : state->Kr[supp[0]];
    }else{                     // average the relative permeability
      Kr = 0.5*(state->Kr[supp[1]]+state->Kr[supp[0]]);
    }
    DarcyFlux = -state->Ki*Kr/state->mu*(state->Psi[supp[1]]-state->Psi[supp[0]])/dist;

    // Load into the residual
    ierr = DMPlexPointGlobalRef(dm,supp[0],Rarray,&r);CHKERRQ(ierr);
    if(r) *r += DarcyFlux*info->V1[p-pStart1]/info->V0[supp[0]];
    ierr = DMPlexPointGlobalRef(dm,supp[1],Rarray,&r);CHKERRQ(ierr);
    if(r) *r -= DarcyFlux*info->V1[p-pStart1]/info->V0[supp[1]];
  }

  // DSW
  
  // Residual = dh/dt ...
  const PetscInt *top1,*top2;
  PetscInt        i,j,k,n1,n2;
  IS              top1IS,top2IS;
  ierr = DMPlexGetStratumIS(dm,"top",1,&top1IS);CHKERRQ(ierr);
  ierr = ISGetIndices(top1IS,&top1);CHKERRQ(ierr);
  ierr = ISGetLocalSize(top1IS,&n1);CHKERRQ(ierr);
  for(i=0;i<n1;i++){
    p    = top1[i];
    ierr = DMPlexPointGlobalRef(dm,p,Rarray,&r);CHKERRQ(ierr);
    if(r){
      PetscReal *h_t;
      ierr = DMPlexPointGlobalRef(dm,p,Pl_t,&h_t);CHKERRQ(ierr);
      (*r) = (*h_t);
    }
  }

  // ... - sum( Manning velocity * interface area / cell volume )
  ierr = DMPlexGetStratumIS(dm,"top",2,&top2IS);CHKERRQ(ierr);
  ierr = ISGetIndices(top2IS,&top2);CHKERRQ(ierr);
  ierr = ISGetLocalSize(top2IS,&n2);CHKERRQ(ierr);
  for(i=0;i<n2;i++){
    p = top2[i]; 

    // Get support of interface
    const PetscInt *supp;
    ierr = DMPlexGetSupportSize(dm,p,&ss  );CHKERRQ(ierr);
    ierr = DMPlexGetSupport    (dm,p,&supp);CHKERRQ(ierr);
    
    // But I really only want the support that is labeled top
    PetscInt topsupp[2],tss=0;
    for(j=0;j<ss;j++){
      for(k=0;k<n1;k++){
	if(supp[j]==top1[k]){
	  topsupp[tss] = supp[j];
	  tss += 1;
	}
      }
    }

    // Handle boundary conditions
    if(tss==1){
      continue; // 0 Neumann for now
    }

    // Estimate the Mannings velocity
    PetscReal dH,H0,H1,*h0,*h1,h,*X0,*X1;
    X0   = &(info->X1[(topsupp[0]-pStart1)*dim]);
    X1   = &(info->X1[(topsupp[1]-pStart1)*dim]);
    Waxpy(dim-1,-1,X0,X1,pnt2pnt); dist = Norm(dim-1,pnt2pnt);
    ierr = DMPlexPointGlobalRef(dm,topsupp[0],Pl,&h0);CHKERRQ(ierr);
    ierr = DMPlexPointGlobalRef(dm,topsupp[1],Pl,&h1);CHKERRQ(ierr);
    if (!(h0 && h1)) continue;
    H0   = X0[dim-1] + (*h0);
    H1   = X1[dim-1] + (*h1);
    dH   = H1-H0;
    h    = PetscMax(0,H1>H0 ? *h1 : *h0);
    PetscReal ManningVel = -PetscPowReal(h,2./3.)*PetscSqrtReal(PetscAbsReal(dH/dist))*PetscSign(dH);

    // Load into the residual
    ierr = DMPlexPointGlobalRef(dm,topsupp[0],Rarray,&r);CHKERRQ(ierr);
    if(r) *r += ManningVel*h*info->V2[p-pStart2]/info->V1[topsupp[0]-pStart1];
    ierr = DMPlexPointGlobalRef(dm,topsupp[1],Rarray,&r);CHKERRQ(ierr);
    if(r) *r -= ManningVel*h*info->V2[p-pStart2]/info->V1[topsupp[1]-pStart1];
  }

  // COUPLING -----
  
  


  // Cleanup

  ierr = ISRestoreIndices(top1IS,&top1);CHKERRQ(ierr);
  ierr = ISDestroy(&top1IS);CHKERRQ(ierr);
  ierr = ISRestoreIndices(top2IS,&top2);CHKERRQ(ierr);
  ierr = ISDestroy(&top2IS);CHKERRQ(ierr);
  ierr = VecRestoreArray(Ul  ,&Pl    );CHKERRQ(ierr);
  ierr = VecRestoreArray(Ul_t,&Pl_t  );CHKERRQ(ierr);
  ierr = VecRestoreArray(R   ,&Rarray);CHKERRQ(ierr);
  //ierr = VecView(R,PETSC_VIEWER_STDOUT_WORLD);
  ierr = DMRestoreLocalVector(dm,&Ul  );CHKERRQ(ierr);
  ierr = DMRestoreLocalVector(dm,&Ul_t);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

typedef PetscBool (*TestPoint)(PetscReal v,PetscReal *x,PetscReal *n,PetscInt dim);

#undef __FUNCT__
#define __FUNCT__ "MarkTopBoundary"
/*
  We need to mark the top boundary in two ways. We need the top
  face/edges to assign a dof to them in the vector. We also need the
  top cells/edges/vertices as this is what we will loop over to
  estimate a flux from one face/edge to another.
 */
PetscErrorCode MarkTopBoundary(DM dm,TestPoint test)
{
  PetscFunctionBegin;
  PetscErrorCode ierr;
  PetscInt i,j,dim;
  PetscInt Start1,End1;
  PetscReal volume,centroid[3],normal[3];
  ierr = DMGetDimension(dm,&dim);CHKERRQ(ierr);
  ierr = DMPlexCreateLabel(dm,"top");CHKERRQ(ierr); 
  ierr = DMPlexGetHeightStratum(dm,1,&Start1,&End1);CHKERRQ(ierr);
  for(i=Start1;i<End1;i++){
    const PetscInt *supp;
    PetscInt ss;
    ierr = DMPlexGetSupportSize(dm,i,&ss);CHKERRQ(ierr);
    ierr = DMPlexGetSupport(dm,i,&supp);CHKERRQ(ierr);
    if(ss==1){ 
      ierr = DMPlexComputeCellGeometryFVM(dm,i,&volume,centroid,normal);CHKERRQ(ierr);
      if(test(volume,centroid,normal,dim)){
  	ierr = DMPlexSetLabelValue(dm,"top",i,1);CHKERRQ(ierr);
  	ierr = DMPlexSetLabelValue(dm,"top",supp[0],0);CHKERRQ(ierr);
	const PetscInt *cone;
	PetscInt cs;
	ierr = DMPlexGetCone(dm,i,&cone);CHKERRQ(ierr);
	ierr = DMPlexGetConeSize(dm,i,&cs);CHKERRQ(ierr);
	for(j=0;j<cs;j++){
	  ierr = DMPlexSetLabelValue(dm,"top",cone[j],2);CHKERRQ(ierr);
	}
      }
    }
  }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "OnTheTop"
/* 
   Mesh-specific definition of what it means to be on the top of a mesh
*/
PetscBool OnTheTop(PetscReal volume,PetscReal *centroid,PetscReal *normal,PetscInt dim)
{
  if(fabs(centroid[dim-1]-0.5)<10*PETSC_MACHINE_EPSILON) return PETSC_TRUE;
  return PETSC_FALSE;
}

#undef __FUNCT__
#define __FUNCT__ "InitialCondition"
/*
  Set the liquid pressure to P0 and the surface water height to h0.
 */
PetscErrorCode InitialCondition(DM dm,Vec U,PetscReal P0,PetscReal h0)
{
  PetscFunctionBegin;
  PetscErrorCode ierr;
  PetscScalar   *u,*Pl,*h;
  PetscInt       p,pStart,pEnd;
  ierr = VecGetArray(U,&u);CHKERRQ(ierr);
  ierr = DMPlexGetHeightStratum(dm,0,&pStart,&pEnd);CHKERRQ(ierr);
  for(p=pStart;p<pEnd;p++){
    ierr = DMPlexPointGlobalRef(dm,p,u,&Pl);CHKERRQ(ierr);
    if(Pl) *Pl = P0;
  }
  ierr = DMPlexGetHeightStratum(dm,1,&pStart,&pEnd);CHKERRQ(ierr);
  for(p=pStart;p<pEnd;p++){
    ierr = DMPlexPointGlobalRef(dm,p,u,&h);CHKERRQ(ierr);
    if(h) *h = rand()/((PetscReal)RAND_MAX);
  }
  ierr = VecRestoreArray(U,&u);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "View"
PetscErrorCode View(TS ts,PetscInt steps,PetscReal time,Vec U,void *mctx)
{
  PetscErrorCode ierr;
  ierr = VecView(U,PETSC_VIEWER_STDOUT_WORLD);CHKERRQ(ierr); 
  return 0;
}

#undef __FUNCT__
#define __FUNCT__ "main"
int main(int argc, char **argv)
{

  srand(time(NULL));
  // Initialize
  MPI_Comm          comm;
  PetscErrorCode    ierr;
  PetscMPIInt       rank;
  ierr = PetscInitialize(&argc,&argv,(char*)0,help);CHKERRQ(ierr);
  comm = PETSC_COMM_WORLD;
  ierr = MPI_Comm_rank(comm,&rank);CHKERRQ(ierr);

  // Problem parameters
  AppCtx user;
  user.state.n        = 2.0;        // [-]      pore-size distributions
  user.state.m        = 0.5;        // [-]      1-1/n
  user.state.alpha    = 1.0194e-04; // [1/Pa]   VanGenuchten parameters
  user.state.Sr       = 0.2;        // [-]      residual saturation
  user.state.Ss       = 1.0;        // [-]      saturated saturation
  user.state.rho      = 1000;       // [kg/m^3] density of water
  user.state.mu       = 9.94e-4;    // [Pa s]   viscosity of water
  user.state.Ks       = 6.94e-5;    // [m/min]  saturated hydraulic conductivity
  user.state.porosity = 0.4;        // [-]      porosity
  user.Pr             = 101325;     // [Pa]     reference pressure (atmospheric)
  user.state.Ki       = user.state.Ks*user.state.mu/user.state.rho/9.81/60.; // [m^2] intrinsic permeability
  user.averaging      = 1;

  // Options
  char filename[PETSC_MAX_PATH_LEN] = "../data/simple.e";
  ierr = PetscOptionsBegin(comm,NULL,"Options","");CHKERRQ(ierr);
  ierr = PetscOptionsString("-mesh","Exodus.II filename to read","",filename,filename,sizeof(filename),NULL);CHKERRQ(ierr);
  ierr = PetscOptionsEnd();CHKERRQ(ierr);

  // Create the mesh
  DM        dm,dmDist;
  PetscInt  overlap=1;
  ierr = DMPlexCreateExodusFromFile(comm,filename,PETSC_TRUE,&dm);CHKERRQ(ierr);
  ierr = MarkTopBoundary(dm,OnTheTop);CHKERRQ(ierr);
  ierr = DMSetFromOptions(dm);CHKERRQ(ierr);

  // Tell the DM how degrees of freedom interact
  ierr = DMPlexSetAdjacencyUseCone(dm,PETSC_TRUE);CHKERRQ(ierr);
  ierr = DMPlexSetAdjacencyUseClosure(dm,PETSC_FALSE);CHKERRQ(ierr);

  // Distribute the mesh
  ierr = DMPlexDistribute(dm,overlap,NULL,&dmDist);CHKERRQ(ierr);
  if (dmDist) { ierr = DMDestroy(&dm);CHKERRQ(ierr); dm = dmDist; }
  ierr = DMGetDimension(dm,&(user.dim));CHKERRQ(ierr);

  // Setup the section, 1 dof per volume cell, 1 dof per top surface face
  PetscSection sec;
  PetscInt     p,pStart,pEnd,lbl;
  ierr = PetscSectionCreate(PetscObjectComm((PetscObject)dm),&sec);CHKERRQ(ierr);
  ierr = PetscSectionSetNumFields(sec,2);CHKERRQ(ierr);
  ierr = DMPlexGetChart(dm,&pStart,&pEnd);CHKERRQ(ierr);
  ierr = PetscSectionSetChart(sec,pStart,pEnd);CHKERRQ(ierr);
  ierr = PetscSectionSetFieldName      (sec,0,"LiquidPressure");CHKERRQ(ierr); 
  ierr = PetscSectionSetFieldComponents(sec,0,1);CHKERRQ(ierr);
  ierr = PetscSectionSetFieldName      (sec,1,"SurfaceWater");CHKERRQ(ierr);   
  ierr = PetscSectionSetFieldComponents(sec,1,1);CHKERRQ(ierr);

  // set a dof on all 0-height cells
  ierr = DMPlexGetHeightStratum(dm,0,&pStart,&pEnd);CHKERRQ(ierr);
  for(p=pStart;p<pEnd;p++){
    ierr = PetscSectionSetFieldDof(sec,p,0,1);CHKERRQ(ierr);
    ierr = PetscSectionSetDof     (sec,p  ,1);CHKERRQ(ierr);
  }

  // set a dof on "top" 1-height cells
  ierr = DMPlexGetHeightStratum(dm,1,&pStart,&pEnd);CHKERRQ(ierr);
  for(p=pStart;p<pEnd;p++){
    ierr = DMPlexGetLabelValue(dm,"top",p,&lbl);CHKERRQ(ierr);
    if(lbl<0) continue;
    ierr = PetscSectionSetFieldDof(sec,p,1,1);CHKERRQ(ierr);
    ierr = PetscSectionSetDof     (sec,p,  1);CHKERRQ(ierr);
  }

  ierr = PetscSectionSetUp(sec);CHKERRQ(ierr);
  ierr = DMSetDefaultSection(dm,sec);CHKERRQ(ierr);
  ierr = PetscSectionDestroy(&sec);CHKERRQ(ierr);
  ierr = DMViewFromOptions(dm, NULL, "-dm_view");CHKERRQ(ierr);

  // Setup problem info
  user.g[0] = 0; user.g[1] = 0; user.g[2] = 0; user.g[user.dim-1] = -9.81;
  ierr = MeshInfoCreate(dm,&(user.info));CHKERRQ(ierr);
  ierr = SoilStateCreate(dm,&(user.state));CHKERRQ(ierr);

  // Create a vec for the initial condition (constant pressure)
  Vec U;
  ierr = DMCreateGlobalVector(dm,&U);CHKERRQ(ierr);
  ierr = InitialCondition(dm,U,99000.0,0.1);CHKERRQ(ierr);
  ierr = PetscObjectSetName((PetscObject)U,"RE.DSW.");CHKERRQ(ierr);

  // Create time stepping and solve
  TS ts;
  ierr = TSCreate(comm,&ts);CHKERRQ(ierr);
  ierr = TSSetType(ts,TSBEULER);CHKERRQ(ierr);
  ierr = TSSetIFunction(ts,NULL,RichardsResidual,&user);CHKERRQ(ierr);
  ierr = TSSetDM(ts,dm);CHKERRQ(ierr);
  ierr = TSSetSolution(ts,U);CHKERRQ(ierr);
  ierr = TSMonitorSet(ts,View,NULL,NULL);CHKERRQ(ierr);
  ierr = TSSetDuration(ts,1000000,1000);CHKERRQ(ierr);
  ierr = TSSetFromOptions(ts);CHKERRQ(ierr);
  ierr = TSSetUp(ts);CHKERRQ(ierr);
  ierr = TSSolve(ts,U);CHKERRQ(ierr);

  // Cleanup
  ierr = SoilStateDestroy(&(user.state));CHKERRQ(ierr);
  ierr = MeshInfoDestroy(&(user.info));CHKERRQ(ierr);
  ierr = TSDestroy(&ts);CHKERRQ(ierr);
  ierr = VecDestroy(&U);CHKERRQ(ierr);
  ierr = DMDestroy(&dm);CHKERRQ(ierr);
  ierr = PetscFinalize();CHKERRQ(ierr);
  return(0);
}
