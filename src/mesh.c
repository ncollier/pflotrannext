#include "pflotran.h"

#undef __FUNCT__
#define __FUNCT__ "MeshInfoCreate"
PetscErrorCode MeshInfoCreate(DM dm,MeshInfo *info)
{
  PetscFunctionBegin;
  PetscErrorCode ierr;
  PetscInt       dim,i,p,pStart,pEnd,nCells;
  ierr   = DMGetDimension(dm,&dim);CHKERRQ(ierr);
  ierr   = DMPlexGetHeightStratum(dm,0,&pStart,&pEnd);CHKERRQ(ierr);
  nCells = pEnd-pStart;
  ierr   = PetscMalloc2(nCells*dim*sizeof(PetscReal),&(info->X0),
			nCells    *sizeof(PetscReal),&(info->V0));CHKERRQ(ierr);
  for(p=pStart;p<pEnd;p++){
    i    = p-pStart;
    ierr = DMPlexComputeCellGeometryFVM(dm,p,&(info->V0[i]),&(info->X0[i*dim]),NULL);CHKERRQ(ierr);
  }
  ierr   = DMPlexGetHeightStratum(dm,1,&pStart,&pEnd);CHKERRQ(ierr);
  nCells = pEnd-pStart;
  ierr   = PetscMalloc(nCells*sizeof(PetscReal),&(info->V1));CHKERRQ(ierr);
  for(p=pStart;p<pEnd;p++){
    i    = p-pStart;
    ierr = DMPlexComputeCellGeometryFVM(dm,p,&(info->V1[i]),NULL,NULL);CHKERRQ(ierr);
  }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "MeshInfoDestroy"
PetscErrorCode MeshInfoDestroy(MeshInfo *info)
{
  PetscFunctionBegin;
  PetscErrorCode ierr;
  ierr = PetscFree2(info->X0,info->V0);CHKERRQ(ierr);
  ierr = PetscFree(info->V1);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "MarkTopBoundary"
/*
  We need to mark the top boundary in two ways. We need the top
  face/edges to assign a dof to them in the vector. We also need the
  top cells/edges/vertices as this is what we will loop over to
  estimate a flux from one face/edge to another.
 */
PetscErrorCode MarkTopBoundary(DM dm,TestPoint test)
{
  PetscFunctionBegin;
  PetscErrorCode ierr;
  PetscInt i,j,dim;
  PetscInt Start1,End1;
  PetscReal volume,centroid[3],normal[3];
  ierr = DMGetDimension(dm,&dim);CHKERRQ(ierr);
  ierr = DMPlexCreateLabel(dm,"top");CHKERRQ(ierr); 
  ierr = DMPlexGetHeightStratum(dm,1,&Start1,&End1);CHKERRQ(ierr);
  for(i=Start1;i<End1;i++){
    const PetscInt *supp;
    PetscInt ss;
    ierr = DMPlexGetSupportSize(dm,i,&ss);CHKERRQ(ierr);
    ierr = DMPlexGetSupport(dm,i,&supp);CHKERRQ(ierr);
    if(ss==1){ 
      ierr = DMPlexComputeCellGeometryFVM(dm,i,&volume,centroid,normal);CHKERRQ(ierr);
      if(test(volume,centroid,normal,dim)){
  	ierr = DMPlexSetLabelValue(dm,"top",i,1);CHKERRQ(ierr);
  	ierr = DMPlexSetLabelValue(dm,"top",supp[0],0);CHKERRQ(ierr);
	const PetscInt *cone;
	PetscInt cs;
	ierr = DMPlexGetCone(dm,i,&cone);CHKERRQ(ierr);
	ierr = DMPlexGetConeSize(dm,i,&cs);CHKERRQ(ierr);
	for(j=0;j<cs;j++){
	  ierr = DMPlexSetLabelValue(dm,"top",cone[j],2);CHKERRQ(ierr);
	}
      }
    }
  }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "ESRICreateFromFile"
PetscErrorCode ESRICreateFromFile(const char filename[],ESRI *esri)
{
  PetscFunctionBegin;
  PetscErrorCode ierr;
  PetscInt       i,j;
  FILE          *fp;
  fp = fopen(filename,"r");
  fscanf(fp,"ncols %d\n",&(esri->ncols));
  fscanf(fp,"nrows %d\n",&(esri->nrows));
  ierr = PetscMalloc((esri->nrows*esri->ncols)*sizeof(PetscReal),&(esri->data));CHKERRQ(ierr);
  fscanf(fp,"xllcorner %lf\n",&(esri->xllcorner));
  fscanf(fp,"yllcorner %lf\n",&(esri->yllcorner));
  fscanf(fp,"cellsize %lf\n",&(esri->cellsize));
  fscanf(fp,"NODATA_value %lf\n",&(esri->NODATA_value));
  for(i=esri->nrows-1;i>=0;i--){
    for(j=0;j<esri->ncols-1;j++) fscanf(fp,"%lf ",&(esri->data[i*esri->ncols+j]));
    fscanf(fp,"%lf\n",&(esri->data[(i+1)*esri->ncols-1]));
  }
  fclose(fp);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "ESRIInterpolate"
PetscReal ESRIInterpolate(ESRI *esri,PetscReal *x,PetscBool shift)
{
  PetscInt  r,c;
  if(shift){
    r = (int)round((x[0]-(               +0.5*esri->cellsize))/esri->cellsize);
    c = (int)round((x[1]-(               +0.5*esri->cellsize))/esri->cellsize);
  }else{
    r = (int)round((x[0]-(esri->xllcorner+0.5*esri->cellsize))/esri->cellsize);
    c = (int)round((x[1]-(esri->yllcorner+0.5*esri->cellsize))/esri->cellsize);
  }
  r = PetscMin(PetscMax(0,r),esri->nrows-1);
  c = PetscMin(PetscMax(0,c),esri->ncols-1);
  return esri->data[r*esri->ncols+c];
}

#undef __FUNCT__
#define __FUNCT__ "ESRIDelete"
PetscErrorCode ESRIDelete(ESRI *esri)
{
  PetscFunctionBegin;
  PetscErrorCode ierr;
  ierr = PetscFree(esri->data);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}
