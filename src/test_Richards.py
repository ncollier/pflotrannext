from __future__ import division
from subprocess import Popen,PIPE,STDOUT
import pylab as plt
import numpy as np
import re

names = {0:"upwind",1:"arithmetic"}
fig,ax = plt.subplots(tight_layout=True)

opts = "-ts_type pseudo -ts_dt 1e-2 -ts_max_snes_failures -1"
for mesh in ["tracy.e","tracy_tri.e"]:
    for avg in range(2):
        h = []
        E = []
        for L in range(6):
            cmd = "./Richards %s -dm_refine %d -avg %d -mesh ../data/%s" % (opts,L,avg,mesh)
            print "Running refine %d on mesh %s using %s rel perm..." % (L,mesh,names[avg])
            p   = Popen(cmd,shell=True,stdout=PIPE,stderr=STDOUT)
            output,errors = p.communicate()
            if errors == None:
                lines = output.split("\n")
                for line in lines:
                    m = re.search(r"L2error\s(.*)",line)
                    if m:
                        h.append(14.25/(10.*2**L))
                        E.append(float(m.group(1)))
        p = np.polyfit(np.log10(h),np.log10(E),1)
        ax.loglog(h,E,'-o',label="%s, %s, O(h^%1.2f)" % (mesh,names[avg],p[0]))

ax.legend(loc=2)
ax.grid()
ax.set_xlabel("cell size, h")
ax.set_ylabel("L2 error")
plt.show()
