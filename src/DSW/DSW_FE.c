#include "petiga.h"
#include "esri.h"

PETSC_STATIC_INLINE PetscScalar Dot(PetscInt dim, const PetscScalar *x, const PetscScalar *y) {PetscScalar sum = 0.0; PetscInt d; for (d = 0; d < dim; ++d) sum += x[d]*y[d]; return sum;}

typedef struct {
  PetscReal Cf,epsilon,alpha,gamma;
  ESRI      terrain;
  IGAProbe  probe;
} AppCtx;

#undef  __FUNCT__
#define __FUNCT__ "L2Projection"
PetscErrorCode L2Projection(IGAPoint p,PetscScalar *K,PetscScalar *F,void *ctx)
{
  if (p->atboundary) return 0;
  PetscInt nen  = p->nen;
  PetscInt dim  = p->dim;
  AppCtx  *user = (AppCtx *)(ctx);
  PetscReal x[dim];
  IGAPointFormGeomMap(p,x); 
  const PetscReal *N = (typeof(N)) p->shape[0];
  
  PetscInt a,b;
  for(a=0; a<nen; a++){
    for(b=0; b<nen; b++) K[b*nen+a] = N[a]*N[b];
    F[a] = N[a]*(ESRIInterpolate(&(user->terrain),x,PETSC_TRUE)+0.05);
  }
  return 0;
}

#undef __FUNCT__
#define __FUNCT__ "ProjectElevation"
PetscErrorCode ProjectElevation(IGA iga,Vec U,AppCtx *user)
{
  PetscErrorCode ierr;
  PetscFunctionBegin;
  
  Mat A;
  Vec b;
  ierr = IGACreateMat(iga,&A);CHKERRQ(ierr);
  ierr = IGACreateVec(iga,&b);CHKERRQ(ierr);
  ierr = IGASetFormSystem(iga,L2Projection,user);CHKERRQ(ierr);
  ierr = IGAComputeSystem(iga,A,b);CHKERRQ(ierr);

  KSP ksp;
  ierr = IGACreateKSP(iga,&ksp);CHKERRQ(ierr);
  ierr = KSPSetOperators(ksp,A,A);CHKERRQ(ierr);
  ierr = KSPSetType(ksp,KSPCG);CHKERRQ(ierr);
  ierr = KSPSetOptionsPrefix(ksp,"l2p_");CHKERRQ(ierr);
  ierr = KSPSetFromOptions(ksp);CHKERRQ(ierr);
  ierr = KSPSetTolerances(ksp,1e-8,PETSC_DEFAULT,PETSC_DEFAULT,PETSC_DEFAULT);CHKERRQ(ierr);
  ierr = KSPSolve(ksp,b,U);CHKERRQ(ierr);

  ierr = KSPDestroy(&ksp);CHKERRQ(ierr);
  ierr = MatDestroy(&A);CHKERRQ(ierr);
  ierr = VecDestroy(&b);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef  __FUNCT__
#define __FUNCT__ "ManningsDiffusivity"
PetscReal ManningsDiffusivity(PetscInt dim,PetscReal *x,PetscReal H,PetscReal *H1,PetscReal *ka_H,AppCtx *user) {
  PetscErrorCode ierr;
  PetscReal z,ka,h,H1dH1,a=user->alpha,g=user->gamma,e=user->epsilon,Cf=user->Cf;
  ierr = IGAProbeFormValue(user->probe,&z);CHKERRQ(ierr);
  h     = PetscMax(H-z,0.);
  H1dH1 = Dot(dim,H1,H1);
  ka    = PetscPowReal(h,a)*PetscPowReal(H1dH1+e*e,0.5*(g-1.))/Cf; 
  if(ka_H){
    ka_H[0] = a*PetscPowReal(h,a-1.)*       PetscPowReal(H1dH1+e*e,0.5*(g-1.))/Cf; // missing Nb 
    ka_H[1] =   PetscPowReal(h,a   )*(g-1.)*PetscPowReal(H1dH1+e*e,0.5*(g-3.))/Cf; // missing Dot(N1[b],H1)
  }
  return ka;
}
  
#undef  __FUNCT__
#define __FUNCT__ "Residual"
PetscErrorCode Residual(IGAPoint p,
			PetscReal shift,const PetscScalar *V,
			PetscReal t    ,const PetscScalar *U,
			PetscScalar *R,void *ctx)
{
  PetscErrorCode ierr;
  AppCtx *user = (AppCtx *)ctx;
  PetscInt a,nen = p->nen;
  PetscInt dim = p->dim;

  PetscReal x[dim];
  IGAPointFormGeomMap(p,x); 

  PetscScalar H_t,H,H1[dim];
  IGAPointFormValue(p,V,&H_t);
  IGAPointFormValue(p,U,&H);
  IGAPointFormGrad (p,U,&H1[0]);
  const PetscReal  *N0       = (typeof(N0)) p->shape[0];
  const PetscReal (*N1)[dim] = (typeof(N1)) p->shape[1];

  ierr = IGAProbeSetPoint(user->probe,p->point);CHKERRQ(ierr);
  
  PetscReal ka = ManningsDiffusivity(dim,x,H,H1,NULL,user);
  for(a=0; a<nen; a++) {
    PetscReal N1_dot_H1 = Dot(dim,&(N1[a][0]),H1);
    R[a]= N0[a]*H_t + ka*N1_dot_H1;
  }
  return 0;
}

			      
#undef  __FUNCT__
#define __FUNCT__ "Jacobian"
PetscErrorCode Jacobian(IGAPoint p,
			PetscReal shift,const PetscScalar *V,
			PetscReal t    ,const PetscScalar *U,
			PetscScalar *J,void *ctx)
{
  AppCtx *user = (AppCtx *)ctx;
  PetscInt a,b,nen = p->nen;
  PetscInt dim = p->dim;

  PetscReal x[dim];
  IGAPointFormGeomMap(p,x); 

  PetscScalar H_t,H,H1[dim];
  IGAPointFormValue(p,V,&H_t);
  IGAPointFormValue(p,U,&H);
  IGAPointFormGrad (p,U,&H1[0]);
  const PetscReal  *N0       = (typeof(N0)) p->shape[0];
  const PetscReal (*N1)[dim] = (typeof(N1)) p->shape[1];

  PetscReal ka,ka_H[2];
  ka = ManningsDiffusivity(dim,x,H,H1,ka_H,user);
  for(a=0; a<nen; a++) {
    PetscReal N1_dot_H1 = Dot(dim,N1[a],H1);
    for(b=0; b<nen; b++) {
      J[b*nen+a]  = shift*N0[a]*N0[b];
      J[b*nen+a] += ka*Dot(dim,N1[a],N1[b]);
      J[b*nen+a] += (ka_H[0]*N0[b]+ka_H[1]*Dot(dim,N1[b],H1))*N1_dot_H1;
    }
  }
  return 0;
}

#undef __FUNCT__
#define __FUNCT__ "main"
int main(int argc, char *argv[]) {

  PetscErrorCode  ierr;
  ierr = PetscInitialize(&argc,&argv,0,0);CHKERRQ(ierr);

  AppCtx user;
  user.Cf      = 0.03;
  user.alpha   = 5.0/3.0;
  user.gamma   = 0.5;
  user.epsilon = 1e-4;

  // Read terrain info
  ierr = ESRICreateFromFile("./areac_extended.arc",&(user.terrain));CHKERRQ(ierr);
  
  IGA         iga;
  IGAAxis     axis;
  PetscInt    dim=2;
  ierr = IGACreate(PETSC_COMM_WORLD,&iga);CHKERRQ(ierr);
  ierr = IGASetDim(iga,dim);CHKERRQ(ierr);
  ierr = IGASetDof(iga,1);CHKERRQ(ierr);
  
  ierr = IGAGetAxis(iga,0,&axis);CHKERRQ(ierr);
  ierr = IGAAxisSetDegree(axis,1);CHKERRQ(ierr);
  ierr = IGAAxisInitUniform(axis,151,0,
			    user.terrain.cellsize*user.terrain.nrows,0);CHKERRQ(ierr);
  ierr = IGAGetAxis(iga,1,&axis);CHKERRQ(ierr);
  ierr = IGAAxisSetDegree(axis,1);CHKERRQ(ierr);
  ierr = IGAAxisInitUniform(axis,151,0,
			    user.terrain.cellsize*user.terrain.ncols,0);CHKERRQ(ierr);

  ierr = IGASetFromOptions(iga);CHKERRQ(ierr);
  ierr = IGASetUp(iga);CHKERRQ(ierr);
 
  ierr = IGASetFormIFunction(iga,Residual,&user);CHKERRQ(ierr);
  ierr = IGASetFormIJacobian(iga,Jacobian,&user);CHKERRQ(ierr);

  Vec      Z;
  ierr = IGACreateVec(iga,&Z);CHKERRQ(ierr);
  ierr = ProjectElevation(iga,Z,&user);CHKERRQ(ierr);
  ierr = ESRIDelete(&(user.terrain));CHKERRQ(ierr);  
  ierr = IGAProbeCreate(iga,Z,&(user.probe));CHKERRQ(ierr);
  ierr = IGAProbeSetOrder(user.probe,0);CHKERRQ(ierr);
  ierr = IGAProbeSetCollective(user.probe,PETSC_FALSE);CHKERRQ(ierr);
  
  Vec       U;
  ierr = IGACreateVec(iga,&U);CHKERRQ(ierr);
  ierr = VecCopy(Z,U);CHKERRQ(ierr);
  ierr = VecShift(U,0.02);CHKERRQ(ierr);
  
  TS     ts;
  ierr = IGACreateTS(iga,&ts);CHKERRQ(ierr);
  ierr = TSSetDuration(ts,PETSC_MAX_INT,10.0);CHKERRQ(ierr);
  ierr = TSSetTimeStep(ts,0.1);CHKERRQ(ierr);
  ierr = TSSetType(ts,TSCN);CHKERRQ(ierr);
  ierr = TSSetFromOptions(ts);CHKERRQ(ierr);

  ierr = TSSolve(ts,U);CHKERRQ(ierr);
  ierr = VecDestroy(&U);CHKERRQ(ierr);
  ierr = TSDestroy(&ts);CHKERRQ(ierr);
  ierr = IGADestroy(&iga);CHKERRQ(ierr);
  ierr = PetscFinalize();CHKERRQ(ierr);
  return 0;
}
