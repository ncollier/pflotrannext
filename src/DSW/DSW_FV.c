static char help[] = "\n";
#include "petsc.h"
#include "time.h"

#define DIM 2
PETSC_STATIC_INLINE void Waxpy(PetscInt dim, PetscScalar a, const PetscScalar *x, const PetscScalar *y, PetscScalar *w) {PetscInt d; for (d = 0; d < dim; ++d) w[d] = a*x[d] + y[d];}
PETSC_STATIC_INLINE PetscScalar Dot(PetscInt dim, const PetscScalar *x, const PetscScalar *y) {PetscScalar sum = 0.0; PetscInt d; for (d = 0; d < dim; ++d) sum += x[d]*y[d]; return sum;}
PETSC_STATIC_INLINE PetscReal Norm(PetscInt dim, const PetscScalar *x) {return PetscSqrtReal(PetscAbsScalar(Dot(dim,x,x)));}

typedef struct {
  PetscReal Cf,*X0,*V0,*Z0,*H,*V1,epsilon,alpha,gamma;
  PetscBool upwind;
} AppCtx;

typedef struct {
  PetscInt nrows,ncols;
  PetscReal xllcorner,yllcorner,cellsize,NODATA_value,*data;
} ESRI;

#undef __FUNCT__
#define __FUNCT__ "ESRICreateFromFile"
PetscErrorCode ESRICreateFromFile(const char filename[],ESRI *esri)
{
  PetscFunctionBegin;
  PetscErrorCode ierr;
  PetscInt       i,j;
  FILE          *fp;
  fp = fopen(filename,"r");
  fscanf(fp,"ncols %d\n",&(esri->ncols));
  fscanf(fp,"nrows %d\n",&(esri->nrows));
  ierr = PetscMalloc((esri->nrows*esri->ncols)*sizeof(PetscReal),&(esri->data));CHKERRQ(ierr);
  fscanf(fp,"xllcorner %lf\n",&(esri->xllcorner));
  fscanf(fp,"yllcorner %lf\n",&(esri->yllcorner));
  fscanf(fp,"cellsize %lf\n",&(esri->cellsize));
  fscanf(fp,"NODATA_value %lf\n",&(esri->NODATA_value));
  for(i=esri->nrows-1;i>=0;i--){
    for(j=0;j<esri->ncols-1;j++) fscanf(fp,"%lf ",&(esri->data[i*esri->ncols+j]));
    fscanf(fp,"%lf\n",&(esri->data[(i+1)*esri->ncols-1]));
  }
  fclose(fp);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "ESRIInterpolate"
PetscReal ESRIInterpolate(ESRI *esri,PetscReal *x,PetscBool shift)
{
  PetscInt  r,c;
  if(shift){
    r = (int)round((x[0]-(               +0.5*esri->cellsize))/esri->cellsize);
    c = (int)round((x[1]-(               +0.5*esri->cellsize))/esri->cellsize);
  }else{
    r = (int)round((x[0]-(esri->xllcorner+0.5*esri->cellsize))/esri->cellsize);
    c = (int)round((x[1]-(esri->yllcorner+0.5*esri->cellsize))/esri->cellsize);
  }
  r = PetscMin(PetscMax(0,r),esri->nrows-1);
  c = PetscMin(PetscMax(0,c),esri->ncols-1);
  return esri->data[r*esri->ncols+c];
}

#undef __FUNCT__
#define __FUNCT__ "ESRIDelete"
PetscErrorCode ESRIDelete(ESRI *esri)
{
  PetscFunctionBegin;
  PetscErrorCode ierr;
  ierr = PetscFree(esri->data);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "Residual"
PetscErrorCode Residual(TS ts,PetscReal t,Vec U,Vec U_t,Vec R,void *ctx)
{
  PetscFunctionBegin;
  PetscErrorCode ierr;
  AppCtx        *user = (AppCtx *)ctx;
  DM             dm;
  ierr = TSGetDM(ts,&dm);CHKERRQ(ierr);

  // Load local arrays
  Vec        Ul, Ul_t;
  PetscReal *h,*h_t,*r;
  ierr = DMGetLocalVector(dm,&Ul  );CHKERRQ(ierr);
  ierr = DMGetLocalVector(dm,&Ul_t);CHKERRQ(ierr);
  ierr = DMGlobalToLocalBegin(dm,U  ,INSERT_VALUES,Ul  );CHKERRQ(ierr);
  ierr = DMGlobalToLocalEnd  (dm,U  ,INSERT_VALUES,Ul  );CHKERRQ(ierr);
  ierr = DMGlobalToLocalBegin(dm,U_t,INSERT_VALUES,Ul_t);CHKERRQ(ierr);
  ierr = DMGlobalToLocalEnd  (dm,U_t,INSERT_VALUES,Ul_t);CHKERRQ(ierr);
  ierr = VecGetArray(Ul  ,&h  );CHKERRQ(ierr);
  ierr = VecGetArray(Ul_t,&h_t);CHKERRQ(ierr);
  ierr = VecGetArray(R   ,&r);CHKERRQ(ierr);

  // What are my 0- and 1-height point ranges?
  PetscInt p,pStart0,pEnd0,pStart1,pEnd1; 
  ierr = DMPlexGetHeightStratum(dm,0,&pStart0,&pEnd0);CHKERRQ(ierr);
  ierr = DMPlexGetHeightStratum(dm,1,&pStart1,&pEnd1);CHKERRQ(ierr);

  // Evaluate the residual, first loop over 0-height points my processor owns
  PetscReal *ref;
  for(p=pStart0;p<pEnd0;p++){
    ierr = DMPlexPointGlobalRef(dm,p,r,&ref);CHKERRQ(ierr);
    if(ref){
      (*ref) = h_t[p];
    }
    // (also update the head computation for use in next loop)
    user->H[p] = user->Z0[p] + h[p];
  }

  // ...next over 1-height points
  PetscInt  ss,up;
  PetscReal pnt2pnt[DIM],dist,ManningVel,gradH,hgt;
  for(p=pStart1;p<pEnd1;p++){

    // Get the support of interface
    const PetscInt *supp;
    ierr = DMPlexGetSupportSize(dm,p,&ss  );CHKERRQ(ierr);
    ierr = DMPlexGetSupport    (dm,p,&supp);CHKERRQ(ierr);

    // Handle boundary conditions
    if(ss==1) continue; // no mass flow out of boundaries

    // Estimate Manning's velocity
    Waxpy(DIM,-1,&(user->X0[supp[0]*DIM]),&(user->X0[supp[1]*DIM]),pnt2pnt);dist=Norm(DIM,pnt2pnt);
    gradH = (user->H[supp[1]]-user->H[supp[0]])/dist; 
    up    =  user->H[supp[0]]>user->H[supp[1]] ? supp[0] : supp[1];
    hgt   =  user->upwind ? h[up] : 0.5*(h[supp[0]]+h[supp[1]]);
    ManningVel = -PetscPowReal(PetscMax(0,hgt),user->alpha)/(user->Cf*PetscPowReal(PetscSqr(gradH)+PetscSqr(user->epsilon),0.5*(1.-user->gamma)))*gradH;

    // Load into the residual
    ierr = DMPlexPointGlobalRef(dm,supp[0],r,&ref);CHKERRQ(ierr);
    if(ref){
      (*ref) += ManningVel*user->V1[p-pStart1]/user->V0[supp[0]];
    }
    ierr = DMPlexPointGlobalRef(dm,supp[1],r,&ref);CHKERRQ(ierr);
    if(ref){
      (*ref) -= ManningVel*user->V1[p-pStart1]/user->V0[supp[1]];
    }
  }

  // Cleanup
  ierr = VecRestoreArray(R   ,&r      );CHKERRQ(ierr);
  ierr = VecRestoreArray(Ul  ,&h      );CHKERRQ(ierr);
  ierr = VecRestoreArray(Ul_t,&h_t    );CHKERRQ(ierr);
  ierr = DMRestoreLocalVector(dm,&Ul  );CHKERRQ(ierr);
  ierr = DMRestoreLocalVector(dm,&Ul_t);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "InitialCondition"
PetscErrorCode InitialCondition(DM dm,Vec U,PetscReal mag_noise,ESRI *esri, AppCtx *user)
{
  PetscFunctionBegin;
  PetscErrorCode ierr;
  srand(time(NULL));
  
  // allocate/initialize the 0-height data
  PetscInt i,np,p,pStart,pEnd;
  PetscScalar *h, *hl;
  ierr = DMPlexGetHeightStratum(dm,0,&pStart,&pEnd);CHKERRQ(ierr);
  np   = pEnd-pStart;
  ierr = PetscMalloc4(DIM*np*sizeof(PetscReal),&(user->X0),
		          np*sizeof(PetscReal),&(user->V0),
		          np*sizeof(PetscReal),&(user->Z0),
		          np*sizeof(PetscReal),&(user->H ));CHKERRQ(ierr);
  ierr = VecGetArray(U,&h);CHKERRQ(ierr);  
  for(p=pStart;p<pEnd;p++){
    ierr = DMPlexPointGlobalRef(dm, p, h, &hl);CHKERRQ(ierr);
    if (!hl) continue;
    i = p - pStart;
    ierr = DMPlexComputeCellGeometryFVM(dm,p,&(user->V0[i]),&(user->X0[i*DIM]),NULL);CHKERRQ(ierr);
    user->Z0[i] = ESRIInterpolate(esri,&(user->X0[i*DIM]),PETSC_TRUE);
    hl[0] = 0.01;
  }

  // allocate/initialize the 1-height data
  ierr = DMPlexGetHeightStratum(dm,1,&pStart,&pEnd);CHKERRQ(ierr);
  np   = pEnd-pStart;
  ierr = PetscMalloc(np*sizeof(PetscReal),&(user->V1));CHKERRQ(ierr);
  for(p=pStart;p<pEnd;p++){
    ierr = DMPlexPointGlobalRef(dm, p, h, &hl);CHKERRQ(ierr);
    if (!hl) continue;
    i = p - pStart;
    ierr = DMPlexComputeCellGeometryFVM(dm,p,&(user->V1[i]),NULL,NULL);CHKERRQ(ierr);
  }
  ierr = VecRestoreArray(U,&h);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "LaplacianSmoothing"
PetscErrorCode LaplacianSmoothing(DM dm,Vec U,PetscInt n,AppCtx *user)
{
  /* Alternative idea, currently unused. */
  PetscFunctionBegin;
  PetscErrorCode ierr;
  PetscInt       i,a,p,pStart,pEnd,adjSize,*adj=NULL;
  PetscReal     *h,count,head;
  ierr = DMPlexGetHeightStratum(dm,0,&pStart,&pEnd);CHKERRQ(ierr);
  ierr = VecGetArray(U,&h);CHKERRQ(ierr);
  for(p=pStart;p<pEnd;p++) user->H[p] = user->Z0[p] + h[p]; // ensure depth and head match
  for(i=0;i<n;i++){
    for(p=pStart;p<pEnd;p++){
      adjSize = PETSC_DETERMINE;
      ierr    = DMPlexGetAdjacency(dm,p,&adjSize,&adj);CHKERRQ(ierr);
      count   = 0;
      head    = 0;
      for(a=0;a<adjSize;a++){
	if(h[adj[a]] > 0){ 
	  head  += user->H[adj[a]];
	  count += 1;
	}
      }
      h[p]       = PetscMax(0,head/count-user->Z0[p]);
      user->H[p] = user->Z0[p] + h[p];
    }
  }
  ierr = VecRestoreArray(U,&h);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "StepPseudoTransientContinuation"
PetscErrorCode StepPseudoTransientContinuation(TS ts)
{
  PetscFunctionBegin;
  PetscErrorCode ierr;
  TS             subts;
  DM             dm;
  TSIFunction    func;
  void          *ctx;
  AppCtx        *user;
  Vec            U;
  PetscReal      t0,dt;

  // Fix: Create this TS once and then store it in the AppCtx so it
  // can be reused
  
  // Get info from this TS
  ierr = TSGetDM(ts,&dm);CHKERRQ(ierr);
  ierr = DMTSGetIFunction(dm,&func,&ctx);CHKERRQ(ierr);
  user = (AppCtx *)(ctx);
  ierr = TSGetSolution(ts,&U);CHKERRQ(ierr);
  ierr = TSGetTimeStep(ts,&dt);CHKERRQ(ierr);
  ierr = TSGetTime    (ts,&t0);CHKERRQ(ierr);
  
  // Setup the new TS
  ierr = TSCreate(PetscObjectComm((PetscObject)dm),&subts);CHKERRQ(ierr);
  ierr = TSSetType(subts,TSPSEUDO);CHKERRQ(ierr);
  ierr = TSSetIFunction(subts,NULL,func,user);CHKERRQ(ierr);
  ierr = TSSetDM(subts,dm);CHKERRQ(ierr);
  ierr = TSSetSolution(subts,U);CHKERRQ(ierr);
  ierr = TSSetInitialTimeStep(subts,t0,dt/10.);CHKERRQ(ierr);
  ierr = TSSetDuration(subts,PETSC_MAX_INT,t0+dt);CHKERRQ(ierr);
  ierr = TSSetExactFinalTime(ts,TS_EXACTFINALTIME_INTERPOLATE);CHKERRQ(ierr);
  ierr = TSSetOptionsPrefix(subts,"sub_");CHKERRQ(ierr);
  ierr = TSSetFromOptions(subts);CHKERRQ(ierr);
  ierr = TSSetUp(subts);CHKERRQ(ierr);

  // Solve and cleanup
  ierr = TSSolve(subts,U);CHKERRQ(ierr);
  ierr = TSDestroy(&subts);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "main"
int main(int argc, char **argv)
{
  // Initialize
  MPI_Comm          comm;
  PetscErrorCode    ierr;
  PetscMPIInt       rank;
  ierr = PetscInitialize(&argc,&argv,(char*)0,help);CHKERRQ(ierr);
  comm = PETSC_COMM_WORLD;
  ierr = MPI_Comm_rank(comm,&rank);CHKERRQ(ierr);

  // Set some application context
  AppCtx user;
  user.Cf      = 0.03;        // [s/(m^(1/3)], Manning's friction coefficient
  user.upwind  = PETSC_FALSE;
  user.epsilon = 1e-16;          
  user.alpha   = 5./3.;
  user.gamma   = 0.5;
  
  // Options
  PetscReal mag_noise = 0.0, mesh_resolution = 1.0;
  PetscBool use_continuation = PETSC_FALSE;
  char      esrifile[PETSC_MAX_PATH_LEN] = "./areac_extended.arc";
  ierr = PetscOptionsBegin(comm,NULL,"Options","");CHKERRQ(ierr);
  ierr = PetscOptionsReal  ("-res"     ,"Resolution of the mesh in meters","",
			    mesh_resolution,&mesh_resolution,NULL);CHKERRQ(ierr);
  ierr = PetscOptionsReal  ("-noise"   ,"Magnitude of the random perturbation of the terrain","",
			    mag_noise,&mag_noise,NULL);CHKERRQ(ierr);
  ierr = PetscOptionsReal  ("-epsilon" ,"Magnitude of the regularization parameter","",
			    user.epsilon,&(user.epsilon),NULL);CHKERRQ(ierr);
  ierr = PetscOptionsBool  ("-ptc"     ,"Enable to use pseudo-transient continuation as an initial guess each time step","",
			    use_continuation,&use_continuation,NULL);CHKERRQ(ierr);
  ierr = PetscOptionsBool  ("-upwind"  ,"Enable to resolve interface discontinuities by taking the upwinded value","",
			    user.upwind,&(user.upwind),NULL);CHKERRQ(ierr);
  ierr = PetscOptionsString("-esri"    ,"Name of esri file to use for elevation data","",
			    esrifile,esrifile,sizeof(esrifile),NULL);CHKERRQ(ierr);
  ierr = PetscOptionsEnd();CHKERRQ(ierr);

  // Read in the esri file with the elevation data
  ESRI esri;
  ierr = ESRICreateFromFile(esrifile,&esri);CHKERRQ(ierr);
  
  // Create the mesh
  DM        dm,dmDist;
  PetscInt  overlap=1; //PetscReal xllcorner,yllcorner
  const PetscReal lower[2] = {0,0};
  const PetscReal upper[2] = {esri.cellsize*esri.nrows, esri.cellsize*esri.ncols};
  const PetscInt  edges[2] = {(PetscInt)(esri.cellsize*esri.nrows/mesh_resolution),(PetscInt)(esri.cellsize*esri.ncols/mesh_resolution)};
  ierr = DMPlexCreate(comm,&dm);CHKERRQ(ierr);
  ierr = DMSetDimension(dm,DIM);CHKERRQ(ierr);
  ierr = DMPlexCreateSquareMesh(dm,lower,upper,edges,DM_BOUNDARY_NONE,DM_BOUNDARY_NONE);CHKERRQ(ierr);
  
  // Tell the DM how degrees of freedom interact
  ierr = DMPlexSetAdjacencyUseCone   (dm,PETSC_TRUE );CHKERRQ(ierr);
  ierr = DMPlexSetAdjacencyUseClosure(dm,PETSC_FALSE);CHKERRQ(ierr);

  // Distribute the mesh
  ierr = DMPlexDistribute(dm,overlap,NULL,&dmDist);CHKERRQ(ierr);
  if (dmDist) { ierr = DMDestroy(&dm);CHKERRQ(ierr); dm = dmDist; }
  ierr = DMViewFromOptions(dm,NULL,"-dm_view");CHKERRQ(ierr);
  
  // Setup the section
  PetscSection sec;
  PetscInt     p,pStart,pEnd;
  ierr = PetscSectionCreate(PetscObjectComm((PetscObject)dm),&sec);CHKERRQ(ierr);
  ierr = PetscSectionSetNumFields(sec,1);CHKERRQ(ierr);
  ierr = DMPlexGetChart(dm,&pStart,&pEnd);CHKERRQ(ierr);
  ierr = PetscSectionSetChart(sec,pStart,pEnd);CHKERRQ(ierr);
  ierr = PetscSectionSetFieldName      (sec,0,"Depth");CHKERRQ(ierr);
  ierr = PetscSectionSetFieldComponents(sec,0,1);CHKERRQ(ierr);

  // Set 1 dof on all 0-height cells
  ierr = DMPlexGetHeightStratum(dm,0,&pStart,&pEnd);CHKERRQ(ierr);
  for(p=pStart;p<pEnd;p++){
    ierr = PetscSectionSetFieldDof(sec,p,0,1);CHKERRQ(ierr);
    ierr = PetscSectionSetDof     (sec,p  ,1);CHKERRQ(ierr);
  }
  ierr = PetscSectionSetUp(sec);CHKERRQ(ierr);
  ierr = DMSetDefaultSection(dm,sec);CHKERRQ(ierr);
  ierr = PetscSectionDestroy(&sec);CHKERRQ(ierr);

  // HACK: We aren't using the DS (discrete system) but it gets
  // attached to the DM and used when determining adjacency for the
  // system matrix. Thus we set correct adjacency here as a temporary
  // fix.
  PetscDS ds;
  ierr = DMGetDS(dm,&ds);CHKERRQ(ierr);
  ierr = PetscDSSetContext(ds,1,NULL);CHKERRQ(ierr);
  ierr = PetscDSSetAdjacency(ds,0,PETSC_TRUE,PETSC_FALSE);CHKERRQ(ierr);
  ierr = PetscDSSetAdjacency(ds,1,PETSC_TRUE,PETSC_FALSE);CHKERRQ(ierr);
  
  // Create a vec for the initial condition (constant pressure)
  Vec U;
  ierr = DMCreateGlobalVector(dm,&U);CHKERRQ(ierr);
  ierr = InitialCondition(dm,U,mag_noise,&esri,&user);CHKERRQ(ierr);
  ierr = PetscObjectSetName((PetscObject)U,"SurfaceWater");CHKERRQ(ierr);
  
  // Create time stepping and solve
  TS ts;
  ierr = TSCreate(comm,&ts);CHKERRQ(ierr);
  ierr = TSSetType(ts,TSBEULER);CHKERRQ(ierr);
  ierr = TSSetIFunction(ts,NULL,Residual,&user);CHKERRQ(ierr);
  ierr = TSSetDM(ts,dm);CHKERRQ(ierr);
  ierr = TSSetSolution(ts,U);CHKERRQ(ierr);
  ierr = TSSetDuration(ts,PETSC_MAX_INT,20);CHKERRQ(ierr);
  if(use_continuation) { ierr = TSSetPreStep(ts,StepPseudoTransientContinuation);CHKERRQ(ierr); }
  ierr = TSSetFromOptions(ts);CHKERRQ(ierr);
  ierr = TSSetUp(ts);CHKERRQ(ierr);
  ierr = TSSolve(ts,U);CHKERRQ(ierr);

  // Cleanup
  ierr = ESRIDelete(&esri);CHKERRQ(ierr);
  ierr = PetscFree5(user.X0,user.V0,user.Z0,user.H,user.V1);CHKERRQ(ierr);
  ierr = TSDestroy(&ts);CHKERRQ(ierr);
  ierr = VecDestroy(&U);CHKERRQ(ierr);
  ierr = DMDestroy(&dm);CHKERRQ(ierr);
  ierr = PetscFinalize();CHKERRQ(ierr);
  return(0);
}
