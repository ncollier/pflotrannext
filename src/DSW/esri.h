#ifndef _ESRI_H_
#define _ESRI_H_

#include "petsc.h"

typedef struct {
  PetscInt  nrows,ncols;
  PetscReal xllcorner,yllcorner,cellsize,NODATA_value,*data;
} ESRI;

PETSC_EXTERN PetscErrorCode ESRICreateFromFile(const char filename[],ESRI *esri);
PETSC_EXTERN PetscErrorCode ESRIDelete(ESRI *esri);
PETSC_EXTERN PetscReal      ESRIInterpolate(ESRI *esri,PetscReal *x,PetscBool shift);

#endif
