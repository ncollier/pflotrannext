#include "esri.h"

#undef __FUNCT__
#define __FUNCT__ "ESRICreateFromFile"
PetscErrorCode ESRICreateFromFile(const char filename[],ESRI *esri)
{
  PetscFunctionBegin;
  PetscErrorCode ierr;
  PetscInt       i,j,num;
  FILE          *fp;
  fp = fopen(filename,"r");
  if(!fp){
    SETERRQ1(PETSC_COMM_WORLD,PETSC_ERR_FILE_OPEN,"Name of file we attempted to open: %s",filename);
  }
  fscanf(fp,"ncols %d\n",&(esri->ncols)); 
  fscanf(fp,"nrows %d\n",&(esri->nrows)); 
  fscanf(fp,"xllcorner %lf\n",&(esri->xllcorner)); 
  fscanf(fp,"yllcorner %lf\n",&(esri->yllcorner)); 
  fscanf(fp,"cellsize %lf\n",&(esri->cellsize)); 
  num = fscanf(fp,"NODATA_value %lf\n",&(esri->NODATA_value));
  if(num != 1){
    SETERRQ1(PETSC_COMM_WORLD,PETSC_ERR_FILE_OPEN,"Error in reading data in file: %s",filename);
  }
  ierr = PetscMalloc((esri->nrows*esri->ncols)*sizeof(PetscReal),&(esri->data));CHKERRQ(ierr);
  for(i=esri->nrows-1;i>=0;i--){
    for(j=0;j<esri->ncols-1;j++) {
      num = fscanf(fp,"%lf ",&(esri->data[i*esri->ncols+j]));
      if(num != 1){
	SETERRQ1(PETSC_COMM_WORLD,PETSC_ERR_FILE_OPEN,"Error in reading data in file: %s",filename);
      }
    }
    num = fscanf(fp,"%lf\n",&(esri->data[i*esri->ncols+j])); 
    if(num != 1){
      SETERRQ1(PETSC_COMM_WORLD,PETSC_ERR_FILE_OPEN,"Error in reading data in file: %s",filename);
    }
  }
  fclose(fp);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "ESRIInterpolate"
PetscReal ESRIInterpolate(ESRI *esri,PetscReal *x,PetscBool shift)
{
  PetscInt  r,c;
  if(shift){
    r = (int)round((x[0]-(               +0.5*esri->cellsize))/esri->cellsize);
    c = (int)round((x[1]-(               +0.5*esri->cellsize))/esri->cellsize);
  }else{
    r = (int)round((x[0]-(esri->xllcorner+0.5*esri->cellsize))/esri->cellsize);
    c = (int)round((x[1]-(esri->yllcorner+0.5*esri->cellsize))/esri->cellsize);
  }
  r = PetscMin(PetscMax(0,r),esri->nrows-1);
  c = PetscMin(PetscMax(0,c),esri->ncols-1);
  return esri->data[r*esri->ncols+c];
}

#undef __FUNCT__
#define __FUNCT__ "ESRIDelete"
PetscErrorCode ESRIDelete(ESRI *esri)
{
  PetscFunctionBegin;
  PetscErrorCode ierr;
  ierr = PetscFree(esri->data);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}
