static char help[] = "\n";
#include "pflotran.h"
#include "time.h"

#undef __FUNCT__
#define __FUNCT__ "InverseLookup"
PetscInt InverseLookup(PetscInt n,const PetscInt *array,PetscInt value)
{
  PetscInt i;
  for(i=0;i<n;i++) if(array[i]==value) return i;
  return -1;
}

#undef __FUNCT__
#define __FUNCT__ "Residual"
PetscErrorCode Residual(TS ts,PetscReal t,Vec U,Vec U_t,Vec R,void *ctx)
{
  PetscFunctionBegin;
  PetscErrorCode ierr;
  AppCtx        *user    = (AppCtx *)ctx;
  SurfaceState  *surface = &(user->surface);
  MeshInfo      *info    = &(user->info);
  DM             dm;
  ierr = TSGetDM(ts,&dm);CHKERRQ(ierr);

  // Global --> Local of the solution and its time derivative
  Vec          Ul, Ul_t;
  PetscScalar *ul,*ul_t;
  ierr = DMGetLocalVector(dm,&Ul  );CHKERRQ(ierr);
  ierr = DMGetLocalVector(dm,&Ul_t);CHKERRQ(ierr);
  ierr = DMGlobalToLocalBegin(dm,U  ,INSERT_VALUES,Ul  );CHKERRQ(ierr);
  ierr = DMGlobalToLocalEnd  (dm,U  ,INSERT_VALUES,Ul  );CHKERRQ(ierr);
  ierr = DMGlobalToLocalBegin(dm,U_t,INSERT_VALUES,Ul_t);CHKERRQ(ierr);
  ierr = DMGlobalToLocalEnd  (dm,U_t,INSERT_VALUES,Ul_t);CHKERRQ(ierr);
  ierr = VecGetArray(Ul  ,&ul  );CHKERRQ(ierr);
  ierr = VecGetArray(Ul_t,&ul_t);CHKERRQ(ierr);

  PetscInt     pStart0,pEnd0,pStart1,pEnd1,pStart2,pEnd2;
  PetscInt     p,ss,dim = user->dim;
  PetscReal   *Rarray,*r,pnt2pnt[3],dist;
  ierr = VecGetArray(R,&Rarray);CHKERRQ(ierr);
  ierr = DMPlexGetHeightStratum(dm,0,&pStart0,&pEnd0);CHKERRQ(ierr);
  ierr = DMPlexGetHeightStratum(dm,1,&pStart1,&pEnd1);CHKERRQ(ierr);
  ierr = DMPlexGetHeightStratum(dm,2,&pStart2,&pEnd2);CHKERRQ(ierr);

  // Subsurface mass/energy balance -----------------------------------------

  // Residual = d/dt( porosity * S ) ...
  for(p=pStart0;p<pEnd0;p++){
    ierr = DMPlexPointGlobalRef(dm,p,Rarray,&r);CHKERRQ(ierr);
    if(r){
      PetscReal *S,*S_t;
      ierr = DMPlexPointGlobalRef(dm,p,ul  ,&S  );CHKERRQ(ierr);
      ierr = DMPlexPointGlobalRef(dm,p,ul_t,&S_t);CHKERRQ(ierr);
      r[0] = S[0]-1.0;
      r[1] = S[1]-10.0;
    }
  }

  // Surface mass/energy balance -------------------------------------------- 
  ierr = SurfaceStateEvaluate(dm,&(ul[2*(pEnd0-pStart0)]),surface,user);CHKERRQ(ierr);

  // Accumulation term
  PetscInt        i,j,k;
  for(i=0;i<info->n1;i++){
    p    = info->top1[i];
    ierr = DMPlexPointGlobalRef(dm,p,Rarray,&r);CHKERRQ(ierr);
    if(r){
      PetscReal *S,*S_t;
      ierr  = DMPlexPointGlobalRef(dm,p,ul  ,&S  );CHKERRQ(ierr);
      ierr  = DMPlexPointGlobalRef(dm,p,ul_t,&S_t);CHKERRQ(ierr);

      // Mass residual, d/dt(eta_l*h_l + eta_i*h_i) + ...
      //r[0]  =  surface->etal_T[i]*S_t[1]*surface->hl[i]; // liquid
      //r[0] +=  surface->etal[i]*(-surface->xi_T[i]*S_t[1]*S[0]+(1-surface->xi[i])*S_t[0]);
      //r[0] +=  surface->etai_T[i]*S_t[1]*surface->hi[i]; // ice
      //r[0] +=  surface->etai[i]*( surface->xi_T[i]*S_t[1]*S[0]+(  surface->xi[i])*S_t[0]);

      // Energy residual, d/dt(eta_l*U_l*h_l + eta_i*U_i*h_i) + ...
      r[1]  = S[1]-10.0;
      /* r[1]  = surface->etal_T[i]*S_t[1]*surface->Ul[i]*surface->hl[i]; // liquid */
      /* r[1] += surface->etal[i]*surface->Ul_T[i]*S_t[1]*surface->hl[i]; */
      /* r[1] += surface->etal[i]*surface->Ul[i]*(-hi_t); */
      /* r[1] += surface->etai_T[i]*S_t[1]*surface->Ui[i]*surface->hi[i]; // ice */
      /* r[1] += surface->etai[i]*surface->Ui_T[i]*S_t[1]*surface->hi[i]; */
      /* r[1] += surface->etai[i]*surface->Ui[i]*hi_t; */

    }
  }

  // ... - sum( Manning velocity * interface area / cell volume )
  for(i=0;i<info->n2;i++){
    p = info->top2[i];

    // Get support of interface
    const PetscInt *supp;
    ierr = DMPlexGetSupportSize(dm,p,&ss  );CHKERRQ(ierr);
    ierr = DMPlexGetSupport    (dm,p,&supp);CHKERRQ(ierr);
    
    // But I really only want the support that is labeled top
    PetscInt topsupp[2],tss=0;
    for(j=0;j<ss;j++){
      for(k=0;k<info->n1;k++){
  	if(supp[j]==info->top1[k]){
  	  topsupp[tss] = supp[j];
  	  tss += 1;
  	}
      }
    }

    // Handle surface boundary conditions
    if(tss==1){
      /* ierr = DMPlexPointGlobalRef(dm,topsupp[0],Rarray,&r);CHKERRQ(ierr); */
      /* if(r){ */
      /* 	k = InverseLookup(info->n1,info->top1,topsupp[0]); */
      /* 	PetscReal dh = surface->hl[k]-0.0; */
      /* 	r[0] += dh; */
      /* } */
      continue;
    }

    // Estimate the Mannings velocity
    PetscReal dH,H0,H1,h0,h1,T0,T1,dT,*S1,*S0,*X0,*X1;
    X0   = &(info->X1[(topsupp[0]-pStart1)*dim]);
    X1   = &(info->X1[(topsupp[1]-pStart1)*dim]);

    Waxpy(dim-1,-1,X0,X1,pnt2pnt); dist = Norm(dim-1,pnt2pnt);
    ierr = DMPlexPointGlobalRef(dm,topsupp[0],ul,&S0);CHKERRQ(ierr);
    ierr = DMPlexPointGlobalRef(dm,topsupp[1],ul,&S1);CHKERRQ(ierr);
    if (!(S0 && S1)) continue;
    h0 = S0[0]; T0 = S0[1];
    h1 = S1[0]; T1 = S1[1];
    H0 = X0[dim-1] + (h0);
    H1 = X1[dim-1] + (h1);
    dH = H1-H0;
    dT = T1-T0;

    // Find state vector index from the point
    PetscInt i0 = InverseLookup(info->n1,info->top1,topsupp[0]);
    PetscInt i1 = InverseLookup(info->n1,info->top1,topsupp[1]);

    // Upwind stuff
    j = (H1>H0 ? i1 : i0);
    PetscReal hl,hi,xi,kappa,etal,Ul,ManningVel,MassFlux,EnergyFlux;
    hl          =  PetscMax(0,surface->hl[j]);
    etal        =  surface->etal[j];
    ManningVel  = -PetscPowReal(hl,2./3.)/surface->Cf*PetscSqrtReal(PetscAbsReal(dH/dist))*PetscSign(dH);
    MassFlux    =  etal*ManningVel*hl; // flux from 0 to 1
    
    printf("X = [%+f %+f], H0[%d] = [%+f %+f], H1[%d] = [%+f %+f], vM = %+f\n",
	   0.5*(X0[0]+X1[0]),0.5*(X0[1]+X1[1]),
	   i0,H0,h0,i1,H1,h1,MassFlux);

    hi          =  PetscMax(0,surface->hi[j]);
    xi          =  PetscMax(0,surface->xi[j]);
    kappa       =  xi*surface->kappai + (1.-xi)*surface->kappal;
    Ul          =  surface->Ul[j];
    EnergyFlux  =  etal*(Ul+PREF/etal)*ManningVel*hl;
    EnergyFlux += -kappa*dT/dist*(hl+hi);

    // Load into the residual
    ierr = DMPlexPointGlobalRef(dm,topsupp[0],Rarray,&r);CHKERRQ(ierr);
    if(r){
      PetscReal AbyV = info->V2[p-pStart2]/info->V1[topsupp[0]-pStart1];
      r[0] += MassFlux*AbyV;
      //r[1] += EnergyFlux*AbyV;
    }
    ierr = DMPlexPointGlobalRef(dm,topsupp[1],Rarray,&r);CHKERRQ(ierr);
    if(r){
      PetscReal AbyV = info->V2[p-pStart2]/info->V1[topsupp[1]-pStart1];
      r[0] -= MassFlux*AbyV;
      //r[1] -= EnergyFlux*AbyV;
    }
  }

  // COUPLING -----

  /* for(i=0;i<21;i++){ */
  /*   printf("%d u = [%e %e], r = [%e %e]\n",i,ul[2*i],ul[2*i+1],Rarray[2*i],Rarray[2*i+1]); */
  /* } */
  for(i=17;i<21;i++){
    printf("r[%d] = %e\n",i,Rarray[2*i]);
  }

  // Cleanup
  ierr = VecRestoreArray(Ul  ,&ul    );CHKERRQ(ierr);
  ierr = VecRestoreArray(Ul_t,&ul_t  );CHKERRQ(ierr);
  ierr = VecRestoreArray(R   ,&Rarray);CHKERRQ(ierr);
  ierr = DMRestoreLocalVector(dm,&Ul  );CHKERRQ(ierr);
  ierr = DMRestoreLocalVector(dm,&Ul_t);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}


#undef __FUNCT__
#define __FUNCT__ "OnTheTop"
/* 
   Mesh-specific definition of what it means to be on the top of a mesh
*/
PetscBool OnTheTop(PetscReal volume,PetscReal *centroid,PetscReal *normal,PetscInt dim)
{
  if(normal[1]>0.1) return PETSC_TRUE;
  return PETSC_FALSE;
}

#undef __FUNCT__
#define __FUNCT__ "InitialCondition"
/*

 */
PetscErrorCode InitialCondition(DM dm,Vec U,PetscReal P0,PetscReal T0)
{
  PetscFunctionBegin;
  PetscErrorCode ierr;
  PetscScalar   *u,*Pl,*h;
  PetscInt       p,pStart,pEnd;
  ierr = VecGetArray(U,&u);CHKERRQ(ierr);
  ierr = DMPlexGetHeightStratum(dm,0,&pStart,&pEnd);CHKERRQ(ierr);
  for(p=pStart;p<pEnd;p++){
    ierr = DMPlexPointGlobalRef(dm,p,u,&Pl);CHKERRQ(ierr);
    if(Pl){
      Pl[0] = P0;
      Pl[1] = T0; //+ (rand()/((PetscReal)RAND_MAX)-0.5);
    }
  }
  ierr = DMPlexGetHeightStratum(dm,1,&pStart,&pEnd);CHKERRQ(ierr);
  for(p=pStart;p<pEnd;p++){
    ierr = DMPlexPointGlobalRef(dm,p,u,&h);CHKERRQ(ierr);
    if(h){
      h[0] = 0.001*p; //*rand()/((PetscReal)RAND_MAX);
      h[1] = T0; // + (rand()/((PetscReal)RAND_MAX)-0.5);
    }
  }
  ierr = VecRestoreArray(U,&u);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "View"
PetscErrorCode View(TS ts,PetscInt step,PetscReal time,Vec U,void *mctx)
{
  PetscFunctionBegin;
  PetscErrorCode ierr;
  const PetscScalar   *u;
  ierr = VecGetArrayRead(U,&u);CHKERRQ(ierr);
  PetscPrintf(PETSC_COMM_WORLD,"step = %d, t = %e, h = {%1.5f %1.5f}, T = {%2.14f %2.14f}\n",step,time,u[8],u[10],u[9],u[11]);
  ierr = VecRestoreArrayRead(U,&u);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "MonitorMassEnergy"
PetscErrorCode MonitorMassEnergy(TS ts,PetscInt step,PetscReal time,Vec U,void *ctx)
{
  PetscFunctionBegin;
  PetscErrorCode     ierr;
  DM                 dm;
  AppCtx            *user    = (AppCtx *)ctx;
  SurfaceState      *surface = &(user->surface);
  MeshInfo          *info    = &(user->info);
  PetscInt           pStart0,pEnd0,pStart1,pEnd1,p,i;
  const PetscScalar *constUarray;
  PetscScalar       *u,dt,per,local_surface_mass=0,surface_mass=0;
  PetscInt           snes_its,ksp_its;
  ierr = TSGetDM(ts,&dm);CHKERRQ(ierr);
  ierr = DMPlexGetHeightStratum(dm,0,&pStart0,&pEnd0);CHKERRQ(ierr);
  ierr = DMPlexGetHeightStratum(dm,1,&pStart1,&pEnd1);CHKERRQ(ierr);
  ierr = VecGetArrayRead(U,&constUarray);CHKERRQ(ierr);
  PetscScalar *Uarray = (PetscScalar *)constUarray;
  ierr = SurfaceStateEvaluate(dm,&Uarray[2*(pEnd0-pStart0)],surface,user);CHKERRQ(ierr); 
  for(i=0;i<info->n1;i++){
    p = info->top1[i];
    ierr = DMPlexPointGlobalRef(dm,p,Uarray,&u);CHKERRQ(ierr);
    if(u){
      printf("%e ",surface->hl[i]);
      local_surface_mass += (surface->etal[i]*surface->hl[i]+
			     surface->etai[i]*surface->hi[i])*info->V1[p-pStart1];
    }
  }
  printf("\n");
  ierr = VecRestoreArrayRead(U,&constUarray);CHKERRQ(ierr);
  ierr = MPI_Reduce((void *)(&local_surface_mass),
		    (void *)(&surface_mass),
		    1,MPI_DOUBLE,MPI_SUM,0,PETSC_COMM_WORLD);CHKERRQ(ierr);
  ierr = TSGetTimeStep(ts,&dt);CHKERRQ(ierr);
  ierr = TSGetSNESIterations(ts,&snes_its);CHKERRQ(ierr);
  ierr = TSGetKSPIterations(ts,&ksp_its);CHKERRQ(ierr);
  if(step==0){
    user->mass = surface_mass;
  }
  per  = (surface_mass-user->mass)/user->mass*100.0;
  ierr = PetscPrintf(PETSC_COMM_WORLD,"%d TS dt %e time %e snes %d ksp %d mass %1.15e (%+.3f\%)\n",
  		     step,dt,time,snes_its,ksp_its,surface_mass,per);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "main"
int main(int argc, char **argv)
{

  // Initialize
  MPI_Comm          comm;
  PetscErrorCode    ierr;
  PetscMPIInt       rank;
  ierr = PetscInitialize(&argc,&argv,(char*)0,help);CHKERRQ(ierr);
  comm = PETSC_COMM_WORLD;
  ierr = MPI_Comm_rank(comm,&rank);CHKERRQ(ierr);
  srand(time(NULL));

  // Set some application context
  AppCtx user;
  user.surface.Tmin   = -1.0;    // [C]
  user.surface.Tmax   =  1.0;    // [C]
  user.surface.Cf     =  0.03;   // [-]
  user.surface.kappal =  0.6e-3; // [kJ/(s m K)]
  user.surface.kappai =  2.1e-3; // [kJ/(s m K)]

  // Options
  char filename[PETSC_MAX_PATH_LEN] = "../data/bowl.e";
  ierr = PetscOptionsBegin(comm,NULL,"Options","");CHKERRQ(ierr);
  ierr = PetscOptionsString("-mesh","Exodus.II filename to read","",filename,filename,sizeof(filename),NULL);CHKERRQ(ierr);
  ierr = PetscOptionsEnd();CHKERRQ(ierr);

  // Create the mesh
  DM        dm,dmDist;
  PetscInt  overlap=1;
  ierr = DMPlexCreateExodusFromFile(comm,filename,PETSC_TRUE,&dm);CHKERRQ(ierr);
  ierr = MarkTopBoundary(dm,OnTheTop);CHKERRQ(ierr);
  ierr = DMSetFromOptions(dm);CHKERRQ(ierr);

  // Tell the DM how degrees of freedom interact
  ierr = DMPlexSetAdjacencyUseCone   (dm,PETSC_TRUE );CHKERRQ(ierr);
  ierr = DMPlexSetAdjacencyUseClosure(dm,PETSC_FALSE);CHKERRQ(ierr);

  // Distribute the mesh
  ierr = DMPlexDistribute(dm,overlap,NULL,&dmDist);CHKERRQ(ierr);
  if (dmDist) { ierr = DMDestroy(&dm);CHKERRQ(ierr); dm = dmDist; }
  ierr = DMGetDimension(dm,&(user.dim));CHKERRQ(ierr);

  // Setup the section
  PetscSection sec;
  PetscInt     p,pStart,pEnd,lbl;
  ierr = PetscSectionCreate(PetscObjectComm((PetscObject)dm),&sec);CHKERRQ(ierr);
  ierr = PetscSectionSetNumFields(sec,4);CHKERRQ(ierr);
  ierr = DMPlexGetChart(dm,&pStart,&pEnd);CHKERRQ(ierr);
  ierr = PetscSectionSetChart(sec,pStart,pEnd);CHKERRQ(ierr);
  ierr = PetscSectionSetFieldName      (sec,0,"SubsurfaceLiquidPressure");CHKERRQ(ierr); 
  ierr = PetscSectionSetFieldComponents(sec,0,1);CHKERRQ(ierr);
  ierr = PetscSectionSetFieldName      (sec,1,"SubsurfaceTemperature");CHKERRQ(ierr); 
  ierr = PetscSectionSetFieldComponents(sec,1,1);CHKERRQ(ierr);
  ierr = PetscSectionSetFieldName      (sec,2,"SurfaceWaterDepth");CHKERRQ(ierr);   
  ierr = PetscSectionSetFieldComponents(sec,2,1);CHKERRQ(ierr);
  ierr = PetscSectionSetFieldName      (sec,3,"SurfaceWaterTemperature");CHKERRQ(ierr);   
  ierr = PetscSectionSetFieldComponents(sec,3,1);CHKERRQ(ierr);

  // set two dofs on all 0-height cells
  ierr = DMPlexGetHeightStratum(dm,0,&pStart,&pEnd);CHKERRQ(ierr);
  for(p=pStart;p<pEnd;p++){
    ierr = PetscSectionSetFieldDof(sec,p,0,1);CHKERRQ(ierr);
    ierr = PetscSectionSetDof     (sec,p  ,2);CHKERRQ(ierr);
    ierr = PetscSectionSetFieldDof(sec,p,1,1);CHKERRQ(ierr);
    ierr = PetscSectionSetDof     (sec,p  ,2);CHKERRQ(ierr);
  }

  // set a dof on "top" 1-height cells
  ierr = DMPlexGetHeightStratum(dm,1,&pStart,&pEnd);CHKERRQ(ierr);
  for(p=pStart;p<pEnd;p++){
    ierr = DMPlexGetLabelValue(dm,"top",p,&lbl);CHKERRQ(ierr);
    if(lbl<0) continue;
    ierr = PetscSectionSetFieldDof(sec,p,2,1);CHKERRQ(ierr);
    ierr = PetscSectionSetDof     (sec,p,  2);CHKERRQ(ierr);
    ierr = PetscSectionSetFieldDof(sec,p,3,1);CHKERRQ(ierr);
    ierr = PetscSectionSetDof     (sec,p,  2);CHKERRQ(ierr);
  }

  ierr = PetscSectionSetUp(sec);CHKERRQ(ierr);
  ierr = DMSetDefaultSection(dm,sec);CHKERRQ(ierr);
  ierr = PetscSectionDestroy(&sec);CHKERRQ(ierr);
  ierr = DMViewFromOptions(dm, NULL, "-dm_view");CHKERRQ(ierr);

  // Setup problem info
  user.g[0] = 0; user.g[1] = 0; user.g[2] = 0; user.g[user.dim-1] = -9.81;
  ierr = MeshInfoCreate(dm,&(user.info));CHKERRQ(ierr);
  ierr = SoilStateCreate(dm,&(user.soil));CHKERRQ(ierr);
  ierr = SurfaceStateCreate(dm,&(user.surface));CHKERRQ(ierr);

  // Setup some index sets
  ierr = DMPlexGetStratumIS(dm,"top",1,&(user.info.top1IS));CHKERRQ(ierr);
  ierr = ISGetIndices(user.info.top1IS,&(user.info.top1));CHKERRQ(ierr);
  ierr = ISGetLocalSize(user.info.top1IS,&(user.info.n1));CHKERRQ(ierr);
  ierr = DMPlexGetStratumIS(dm,"top",2,&(user.info.top2IS));CHKERRQ(ierr);
  ierr = ISGetIndices(user.info.top2IS,&(user.info.top2));CHKERRQ(ierr);
  ierr = ISGetLocalSize(user.info.top2IS,&(user.info.n2));CHKERRQ(ierr);

  // Create a vec for the initial condition (constant pressure)
  Vec U;
  ierr = DMCreateGlobalVector(dm,&U);CHKERRQ(ierr);
  ierr = InitialCondition(dm,U,1.234,10.0);CHKERRQ(ierr);
  ierr = PetscObjectSetName((PetscObject)U,"CTH");CHKERRQ(ierr);

  // Create time stepping and solve
  TS ts;
  ierr = TSCreate(comm,&ts);CHKERRQ(ierr);
  ierr = TSSetType(ts,TSBEULER);CHKERRQ(ierr);
  ierr = TSSetIFunction(ts,NULL,Residual,&user);CHKERRQ(ierr);
  ierr = TSSetDM(ts,dm);CHKERRQ(ierr);
  ierr = TSSetSolution(ts,U);CHKERRQ(ierr);
  ierr = TSMonitorSet(ts,MonitorMassEnergy,&user,NULL);CHKERRQ(ierr);
  ierr = TSSetDuration(ts,1000,1000000);CHKERRQ(ierr);
  ierr = TSSetFromOptions(ts);CHKERRQ(ierr);
  ierr = TSSetUp(ts);CHKERRQ(ierr);
  ierr = TSSolve(ts,U);CHKERRQ(ierr);

  // Cleanup
  ierr = ISRestoreIndices(user.info.top1IS,&(user.info.top1));CHKERRQ(ierr);
  ierr = ISDestroy(&(user.info.top1IS));CHKERRQ(ierr);
  ierr = ISRestoreIndices(user.info.top2IS,&(user.info.top2));CHKERRQ(ierr);
  ierr = ISDestroy(&(user.info.top2IS));CHKERRQ(ierr);
  ierr = SoilStateDestroy(&(user.soil));CHKERRQ(ierr);
  ierr = SurfaceStateDestroy(&(user.surface));CHKERRQ(ierr);
  ierr = MeshInfoDestroy(&(user.info));CHKERRQ(ierr);
  ierr = TSDestroy(&ts);CHKERRQ(ierr);
  ierr = VecDestroy(&U);CHKERRQ(ierr);
  ierr = DMDestroy(&dm);CHKERRQ(ierr);
  ierr = PetscFinalize();CHKERRQ(ierr);
  return(0);
}
