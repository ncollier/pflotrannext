#include "pflotran.h"


#undef __FUNCT__
#define __FUNCT__ "PressureSaturation_VanGenuchten"
void PressureSaturation_VanGenuchten(PetscReal n,PetscReal m,PetscReal alpha,PetscReal Pc,PetscReal *Se,PetscReal *dSe_dPc)
{
  Pc  = PetscMax(Pc,0);
  *Se = pow(1.+PetscPowReal(alpha*Pc,n),-m);
  if(dSe_dPc) *dSe_dPc = (-m*n*alpha)*PetscPowReal(1.+pow(alpha*Pc,n),-m-1.)*(1.+pow(alpha*Pc,n-1.));
}

#undef __FUNCT__
#define __FUNCT__ "PressureSaturation_Gardner"
void PressureSaturation_Gardner(PetscReal n,PetscReal m,PetscReal alpha,PetscReal Pc,PetscReal *Se,PetscReal *dSe_dPc)
{
  Pc  = PetscMax(Pc,0);
  *Se = PetscExpReal(-alpha*Pc/m);
  if(dSe_dPc) *dSe_dPc = -alpha/m*PetscExpReal(-alpha*Pc/m);
}

#undef __FUNCT__
#define __FUNCT__ "PopulatePressureSaturationFunctions"
PetscErrorCode PopulatePressureSaturationFunctions(PetscFunctionList *Functions)
{
  PetscFunctionBegin;
  PetscErrorCode ierr;
  ierr = PetscFunctionListAdd(Functions,"vangenuchten",PressureSaturation_VanGenuchten);CHKERRQ(ierr);
  ierr = PetscFunctionListAdd(Functions,"gardner"     ,PressureSaturation_Gardner     );CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "RelativePermeability_Mualem"
void RelativePermeability_Mualem(PetscReal m,PetscReal Se,PetscReal *Kr,PetscReal *dKr_dSe)
{
  *Kr = PetscSqrtReal(Se)*PetscSqr(1.-PetscPowReal(1.-PetscPowReal(Se,1./m),m));
  if(dKr_dSe) *dKr_dSe = 0;
}

#undef __FUNCT__
#define __FUNCT__ "RelativePermeability_Irmay"
void RelativePermeability_Irmay(PetscReal m,PetscReal Se,PetscReal *Kr,PetscReal *dKr_dSe)
{
  *Kr = PetscPowReal(Se,m);
  if(dKr_dSe) *dKr_dSe = 0;
}

#undef __FUNCT__
#define __FUNCT__ "FrozenFraction_Conic"
void FrozenFraction_Conic(PetscReal T,PetscReal *xi,PetscReal *xi_T,PetscReal Tmin,PetscReal Tmax)
{
  PetscReal tmp = PetscMin(1.0,PetscMax(0.0,(T-Tmin)/(Tmax-Tmin)));
  (*xi) = PetscSqr(1-PetscSqr(tmp));
  if (xi_T) (*xi_T) = -4.0*(1-PetscSqr(tmp))*tmp/(Tmax-Tmin);
}
