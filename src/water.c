#include "pflotran.h"

/*
  T, temperature [C]
  P, pressure [Pa]
  eta, molar density [kmol/m^3]
  eta_T, derivative of molar density with respect to temperature [kmol/(m^3 C)]
  eta_P, derivative of molar density with respect to pressure [kmol/(m^3 Pa)]
 */
PetscErrorCode IceDensity(PetscReal T,PetscReal P,PetscReal *eta,PetscReal *eta_T,PetscReal *eta_P)
{  
  PetscReal alpha = 3.30e-10;
  PetscReal beta  = 1.53e-4;
  (*eta) = 50.9424*(1+alpha*(P-PREF)-beta*T);
  if (eta_T) (*eta_T) = 50.9424*(-beta);
  if (eta_P) (*eta_P) = 50.9424*(alpha);
  PetscFunctionReturn(0);
} 
 
/*
  T, temperature [C]
  U, internal energy [kJ/kmol]
  U_T, derivative of internal energy with respect to temperature [kJ/(kmol C)]
*/
PetscErrorCode IceInternalEnergy(PetscReal T,PetscReal *U,PetscReal *U_T)
{
  PetscReal TK = T+TREF;
  (*U)  = -10.6644*T;
  (*U) +=  0.5*0.1698*(TK*TK-TREF*TREF);
  (*U) +=  198148.*(1./TREF-1./TK);
  (*U) += -HEAT_OF_FUSION*FMWH2O*1.e-3;
  if (U_T) (*U_T) = 10.6644 + 0.1698*TK + 198148./(TK*TK);
  PetscFunctionReturn(0);
}

/*
  T, temperature [C]
  P, pressure [Pa]
  eta, molar density [kmol/m^3]
  eta_T, derivative of molar density with respect to temperature [kmol/(m^3 C)]
  eta_P, derivative of molar density with respect to pressure [kmol/(m^3 Pa)]
 */
PetscErrorCode WaterDensity(PetscReal T,PetscReal P,PetscReal *eta,PetscReal *eta_T,PetscReal *eta_P)
{
  PetscReal T2=T*T,T3=T*T2;
  (*eta) = (999.915 + 0.0416516*T - 0.0100836*T2 + 0.000206355*T3)*(1.+5e-10*(P-PREF))/FMWH2O;
  if (eta_P) (*eta_P) = 5e-10*(999.915 + 0.0416516*T - 0.0100836*T2 + 0.000206355*T3)/FMWH2O;
  if (eta_T) (*eta_T) =  (0.0416516 - 0.0201672*T + 0.000619065*T2)*(1.+5e-10*(P-PREF))/FMWH2O;
  PetscFunctionReturn(0);
}

/*
  T, temperature [C]
  U, internal energy [kJ/kmol]
  U_T, derivative of internal energy with respect to temperature [kJ/(kmol C)]
 */
PetscErrorCode WaterInternalEnergy(PetscReal T,PetscReal *U,PetscReal *U_T)
{
  (*U) = 4.217*T*FMWH2O;
  if(U_T) (*U_T) = 4.217*FMWH2O;
  PetscFunctionReturn(0);
}
