from __future__ import division
import sys, petsc4py
petsc4py.init(sys.argv)
from petsc4py import PETSc
import numpy as np
import pylab as plt

class DSW(object):
    def __init__(self,N,x=None,z=None,h=None,rain=None,outname="dsw"):
        """
        Create a new diffusive wave approximation to the shallow water
        equation (DSW) object.

        Parameters
        ----------
        N : int
            number of cells
        z : numpy.ndarray, optional
            an array of size N representing the elevation of cells
        h : numpy.ndarray ,optional
            an array of size N representing the initial water height
        rain : function(x,t), optional
            rainfall intensity function
        """
        if x is None:
            self.dx = 1/N
            self.x = np.linspace(self.dx/2,1-self.dx/2,N)
        else:
            self.x = x
            self.dx = x[1]-x[0]
        self.r = PETSc.Vec().createSeq(N)
        self.h = PETSc.Vec().createSeq(N)
        if h is not None:
            self.h[...] = h
            self.h.assemble()
        if z is None:
            self.z = np.zeros(N)
        else:
            self.z = z
        self.rain = rain
        self.Cf   = 0.03
        self.outname = outname
        
    def __call__(self,ts,step,t,h):
        X = self.x[...]
        Z = self.z[...]
        H = self.h[...]
        fig,ax = plt.subplots(figsize=(20,2),tight_layout=True)
        ax.fill_between(X,0,Z  ,color='g',linewidth=0)
        ax.fill_between(X,Z,H+Z,color='b',linewidth=0)
        ax.set_xlim(0,10)
        ax.set_ylim(0,0.6)
        fig.savefig("%s%d.png" % (self.outname,step))
        plt.close()
        
    def evalResidual(self,ts,t,h,hdot,r):        
        r[...]  =  hdot
        flux    =  self.flux(h[...])
        r[:-1] += +flux/self.dx
        r[+1:] += -flux/self.dx
        r.assemble()

    def flux(self,h):
        H   =  h+self.z                   # head           
        dH  = (H[1:]-H[:-1])              # head gradient 
        hup = (dH<0)*h[:-1]+(dH>0)*h[1:]  # upwinded water height (lame, I know)
        
        # Manning's velocity and mass flux
        vel  = -np.power(hup.clip(0),2/3)/self.Cf*np.sqrt(np.abs(dH/self.dx))*np.sign(dH)
        flux =  vel*hup
        return flux

def Test1():
    N   =  11
    X   =  np.linspace(0,10,N)
    Z   =  0.01*(X-5.)**2
    H   = -2*(X-9)**2+0.4
    H   =  (H>Z)*(H-Z) + (H<Z)*0
    dsw =  DSW(N,x=X,z=Z,h=H,outname="Test1_")
    ts  =  PETSc.TS().create()    
    ts.setType(PETSc.TS.Type.BEULER)
    ts.setIFunction(dsw.evalResidual,dsw.r)
    ts.setMaxTime(3600)
    ts.setMaxSteps(100)
    ts.setMonitor(dsw)
    ts.setFromOptions()
    ts.solve(dsw.h)

def Test2():
    N   =  101
    X   =  np.linspace(0,10,N)
    Z   =  0.01*(X-5.)**2
    Z  +=  np.random.rand(N)*0.08
    H   = -2*(X-9)**2+0.4
    H   =  (H>Z)*(H-Z) + (H<Z)*0
    dsw =  DSW(N,x=X,z=Z,h=H,outname="Test2_")
    ts  =  PETSc.TS().create()    
    ts.setType(PETSc.TS.Type.BEULER)
    ts.setIFunction(dsw.evalResidual,dsw.r)
    ts.setMaxTime(3600)
    ts.setMaxSteps(100)
    ts.setMonitor(dsw)
    ts.setFromOptions()
    ts.solve(dsw.h)

Test1() # Idealized problem common in papers and benchmark tests
#Test2() # Reality of terrain data, Test1 with noise
