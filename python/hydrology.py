from __future__ import division
import numpy as np

def VanGenuchten(n,m,alpha,Pc=None,Se=None,derivative=False):
    """
    Defines the VanGenuchten pressure-saturation relationship and its inverse.

    Se = ( 1 + (alpha*Pc)^n )^(-m)

    Parameters:
    -----------
    n : float
        a VanGenuchten parameter
    m : float
        a VanGenuchten parameter
    alpha : float
        a VanGenuchten parameter
    Pc : ndarray (optional)
        the capillary pressure, will be clipped at Pc = 0
    Se : ndarray (optional)
        the effective saturation
    derivative : boolean
        enable to return the derivative, dSe/dPc
        
    Returns:
    --------
    Se : ndarray
        the effective saturation if Pc is specified
    Pc : ndarray
        the capillary pressure if Se is specified
    None :
        if neither Pc or Se is specified
    """
    if Pc != None:
        Pc = np.asarray(Pc)
        Se = (1+(alpha*Pc.clip(0))**n)**(-m)
        if derivative:
            dSe = (-m*n*alpha)*(1+(alpha*Pc.clip(0))**n)**(-m-1)*(1+(alpha*Pc.clip(0))**(n-1))
            return Se,dSe
        return Se
    if Se != None:
        return ((Se**(-1/m)-1)**(1/n))/alpha
    return None

def BrooksCorey(n,m,alpha,Pc=None,Se=None):
    """
    Defines the BrooksCorey pressure-saturation relationship and its inverse.

    Se = ( alpha*Pc )^(-m*n)

    Parameters:
    -----------
    n : float
        a BrooksCorey parameter
    m : float
        a BrooksCorey parameter
    alpha : float
        a BrooksCorey parameter
    Pc : ndarray (optional)
        the capillary pressure, will be clipped at Pc = 0
    Se : ndarray (optional)
        the effective saturation

    Returns:
    --------
    Se : ndarray
        the effective saturation if Pc is specified
    Pc : ndarray
        the capillary pressure if Se is specified
    None :
        if neither Pc or Se is specified
    """
    lamda = n*m
    if Pc != None:
        Pc = np.asarray(Pc).clip(0)
        return (alpha*Pc)**(-lamda)
    if Se != None:
        return (Se**(-1/lamda))/alpha
    return None

def RelativePermeabilityMualem(Se,n,derivative=False):
    """
    Computes the relative permeability as given by Mualem.
    
    Parameters:
    -----------
    n : float
        a VanGenuchten parameter
    Se : ndarray
        the effective saturation

    Returns:
    --------
    kr : ndarray
        the relative permeability
    """
    m = 1-1/n
    assert 0<m<1
    Se = np.asarray(Se)
    Kr = np.sqrt(Se)*(1-(1-Se**(1/m))**m)**2
    if derivative:
        dSe = Se[1]-Se[0]
        dKr = np.zeros(Kr.shape)
        dKr[1:-1] = 0.5*(Kr[:-2]-2*Kr[1:-1]+Kr[2:])/dSe
        dKr[ 0] = (Kr[ 1]-Kr[ 0])/dSe
        dKr[-1] = (Kr[-1]-Kr[-2])/dSe
        return Kr,dKr
    return Kr

if __name__ == "__main__":
    """
    This comparison appears in the VanGenuchten1980 paper
    """
    import pylab as plt
    fig,ax = plt.subplots(figsize=(13,6),ncols=3,tight_layout=True)
    
    Ss    = 0.5
    Sr    = 0.1
    n     = 2
    m     = 0.5
    alpha = 0.005

    eps = 1e-12
    Se  = np.linspace(eps,1-eps,1000)
    S   = Se*(Ss-Sr)+Sr

    for Sfun,lbl,mark in zip([VanGenuchten,BrooksCorey],['VanGenuchten','BrooksCorey'],['-r','--k']):
        Pc = Sfun(n,m,alpha,Se=Se)
        ax[0].semilogy(S,Pc,mark,label=lbl,lw=2)
        ax[1].loglog(Pc,RelativePermeabilityMualem(Se,n),mark,lw=2)
    ax[0].set_ylabel("Capillary Pressure [Pa]")
    ax[0].set_xlabel("Saturation [-]")
    ax[0].set_xlim((0,.6))
    ax[0].set_ylim((1e0,1e6))
    ax[0].legend(loc=3)
    ax[1].set_xlabel("Capillary Pressure [Pa]")
    ax[1].set_ylabel("Relative Permeability [-]")
    ax[1].set_xlim((1e0,1e4))
    ax[1].set_ylim((1e-6,1e0))
    ax[2].semilogy(S,RelativePermeabilityMualem(Se,n),'-r',lw=2)
    ax[2].set_xlabel("Saturation [-]")
    ax[2].set_ylabel("Relative Permeability [-]")
    ax[2].set_xlim((0,.6))
    plt.show()
