from __future__ import division
import sys, petsc4py
petsc4py.init(sys.argv)
from petsc4py import PETSc
import numpy as np
import pylab as plt
import hydrology as hyd
import fv as fv
from plot import PlotPlex,PlotFlux

saturation    = hyd.VanGenuchten
relative_perm = hyd.RelativePermeabilityMualem
flux_approx   = fv.ApproximateFluxTwoPoint

def IceModel1(Pl,T,Pr,SaturationFunction,n,m,alpha):
    """
    Pl, liquid pressure [Pa]
    T, temperature [C]
    """
    density_ice        = 9.167e2 # [kg/m3] @ 273.15K
    latent_heat_fusion = 3.34e5  # [J/kg]  @ 273.15K
    T0                 = 273.15  # [K]
    int_tensions_ratio = 2.33    # [non-dim]
    
    Pcil = density_ice*latent_heat_fusion*T/T0
    Pcgl = Pr-Pl
    S1   = SaturationFunction(n,m,alpha,Pc=Pcgl)
    S2   = SaturationFunction(n,m,alpha,Pc=-int_tensions_ratio*Pcil)

    Sl   = 1/(1/S2+1/S1-1)
    Si   = Sl*(1/S2-1)
    Sg   = Sl*(1/S1-1)
    return Sl,Si,Sg

class RichardsFVM():
    
    def __init__(self,dim=2):
        self.dim   = dim
        self.n     = 2            # [-]      VanGenuchten parameters
        self.m     = 1-1/self.n   # [-]      VanGenuchten parameters (compatible with Mualem)
        self.alpha = 0.005        # [1/Pa]   VanGenuchten parameters
        self.Sr    = 0.0          # [-]      residual (minimum possible) saturation
        self.Ss    = 1.0          # [-]      maximum possible saturation
        self.rho   = 1000         # [kg/m^3] density of water
        self.Pr    = 101325       # [Pa]     reference pressure (atmospheric)
        self.mu    = 9.94e-4      # [Pa s]   viscosity of water
        self.ki    = 9.1e-9       # [m^2]    intrinsic permeability
        self.pore  = 0.5          # [-]      porosity
        self.g     = np.zeros(self.dim)
        self.g[-1] = -9.81         # [m/s^2]  gravity
        self.plex  = None
        self.sec   = None

    def createMesh(self,fname=None):
        if fname is not None:
            self.plex = PETSc.DMPlex().createExodusFromFile(fname,comm=PETSc.COMM_WORLD)
        else:
            self.plex = PETSc.DMPlex().createBoxMesh(dim=self.dim,comm=PETSc.COMM_WORLD)
        if self.plex.getComm().getSize() > 1: self.plex.distribute(overlap=1)
        return self

    def createSection(self):
        # Create a section defining 1 degree of freedom per cell
        pStart,pEnd = self.plex.getChart()
        cStart,cEnd = self.plex.getHeightStratum(0)
        self.sec = PETSc.Section().create(self.plex.getComm())
        self.sec.setChart(pStart,pEnd)
        for c in range(cStart,cEnd): sec.setDof(c,1)
        self.sec.setUp()
        self.plex.setDefaultSection(sec)

    def computeMeshInfo(self):
        cStart,cEnd = self.plex.getHeightStratum(0)
        self.xc = np.zeros((cEnd-cStart,self.dim)) # cell centroids
        self.vc = np.zeros(cEnd-cStart)            # cell volumes
        for c in range(cStart,cEnd):
            self.vc[c-cStart],self.xc[c-cStart,:],dummy = self.plex.computeCellGeometryFVM(c)

    def computeState(self,Pl,T):
        Sl,Si,Sg = IceModel1(Pl,T,self.Pr,saturation,self.n,self.m,self.alpha)
        kr  = relative_perm(Sl,self.n)
        Psi = (Pl-self.rho*np.dot(self.xc,self.g))   
        return Sl,Si,Sg,kr,Psi
    
    def computeResidual(self,ts,t,Pl,T,R):
        Sl,Si,Sg,kr,Psi = self.computeState(Pl,T)
        flux = -flux_approx(self.plex,Psi,xc=self.xc,harmonic=self.ki*kr/self.mu)
        return flux

    def plot(self,val,ax):
        cStart,cEnd = self.plex.getHeightStratum(0)
        for c in range(cStart,cEnd):
            ax.text(self.xc[c,0],self.xc[c,1],"%.2f" % val[c-cStart],ha="center",size=8)
        return ax
    
try:
    fv = RichardsFVM().createMesh(sys.argv[1])
except:
    fv = RichardsFVM().createMesh()
fv.computeMeshInfo()

cStart,cEnd = fv.plex.getHeightStratum(0)
Pl   = np.ones(cEnd-cStart)*1.00325e5   # initially constant pressure
#Pl   = fv.rho*fv.g[-1]*(fv.xc[:,-1]-0.1) + 1.01325e5   # hydrostatic
T    = 10*fv.xc[:,1]
flux = fv.computeResidual(None,None,Pl,T,None)

fig,ax = plt.subplots(figsize=(10,10),tight_layout=True)
PlotPlex(fv.plex,ax)
PlotFlux(fv.plex,flux.clip(1e-16),ax)
Sl,Si,Sg,kr,Psi = fv.computeState(Pl,T)
fv.plot(Psi,ax)   
ax.set_xlim((-0.6,0.6))
ax.set_ylim((-0.6,0.6))
plt.show()


