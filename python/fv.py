from __future__ import division
import sys, petsc4py
petsc4py.init(sys.argv)
from petsc4py import PETSc
import numpy as np
import pylab as plt

def ComputeCentroids(plex,stratum=0):
    """
    Compute centroids of given stratum of the plex

    Parameters:
    -----------
    plex : DMPlex
        the PETSc DMPlex context
    scalar : numpy.ndarray
        a scalar array defined on the cells of plex

    Returns:
    --------
    centroids : numpy.ndarray
        an array of the centroids of the stratum, (sEnd-sStart,dim)
    """
    sStart,sEnd = plex.getHeightStratum(stratum)
    xc = np.zeros((sEnd-sStart,plex.getDimension()))
    for p in range(sStart,sEnd):
        dummy,xc[p-sStart,:],dummy = plex.computeCellGeometryFVM(p)
    return xc
            
def ApproximateFluxTwoPoint(plex,scalar,xc=None,upwind=None,harmonic=None):
    """
    This function uses a 2-point flux approximation to return

        DOT(grad(scalar),u)

    where u is the cell center-to-center unit vector.
    
    Parameters:
    -----------
    plex : DMPlex
        the PETSc DMPlex context
    scalar : numpy.ndarray
        a scalar array defined on the cells of plex
    xc : numpy.ndarray (optional)
        optionally provide cell centroids, (ncells,dim)

    Returns:
    --------
    flux : numpy.ndarray
        an array of the gradient of the scalar evaluated at cell interface centroids

    Notes:
    ------
    The flux which is returned is grad(scalar) dotted with the vector connecting cell
    centroids, not the normal at the face/edge shared cells. This induces an error unless
    used on rectangular/hexagonal or equilateral triangular/tetrahedral meshes.
    """
    cStart,cEnd = plex.getHeightStratum(0)
    assert scalar.shape[0] == cEnd-cStart
    if xc == None: xc = ComputeCentroids(plex)
    eStart,eEnd = plex.getHeightStratum(1)
    flux = np.zeros((eEnd-eStart,plex.getDimension()))
    for e in range(eStart,eEnd):
        supp = plex.getSupport(e)
        if supp.shape[0] == 1: continue 
        u = xc[supp[1],:]-xc[supp[0],:]
        magu = np.linalg.norm(u)
        u /= magu
        dscalar_dx = (scalar[supp[1]]-scalar[supp[0]])/magu*u
        flux[e-eStart,:] = dscalar_dx
        if upwind != None:
            flux[e-eStart,:] *= upwind[supp[scalar[supp[1]]>scalar[supp[0]]]]
        if harmonic != None:
            flux[e-eStart,:] *= 2/(1/harmonic[supp[0]]+1/harmonic[supp[1]])
    return flux

if __name__ == "__main__":
    # create a plex
    Ncells = 2
    u      = np.linspace(0,1,Ncells+1)
    X,Y    = np.meshgrid(u,u)
    coords = (np.asarray([X,Y]).T).reshape((-1,2),order='f')
    v0     = (np.outer(range(Ncells),np.ones(Ncells,dtype='int')) +
              np.outer(np.ones(Ncells,dtype='int'),np.asarray(range(Ncells),dtype='int')*(Ncells+1))).flatten(order='f')
    v1     = v0+1
    v2     = v1+Ncells+1
    v3     = v0+Ncells+1
    cells  = np.asarray([v0,v1,v2,v3],dtype='int32').T
    dim    = coords.shape[1]
    plex   = PETSc.DMPlex().createFromCellList(dim,cells,coords,comm=PETSc.COMM_WORLD)
    if plex.getComm().getSize() > 1: plex.distribute(overlap=1)
        
    # plot mesh
    import pylab as plt
    fig,ax = plt.subplots(figsize=(10,10),tight_layout=True)
    eStart,eEnd = plex.getHeightStratum(1)
    for e in range(eStart,eEnd):
        eL,ex,en = plex.computeCellGeometryFVM(e)
        en = en[::-1]; en[0] *= -1
        x = np.zeros((2,dim))
        x[0,:] = ex+0.5*eL*en
        x[1,:] = ex-0.5*eL*en
        ax.plot(x[:,0],x[:,1],'-k',alpha=0.5)

    # make and plot a scalar on cells
    cStart,cEnd = plex.getHeightStratum(0)
    U = np.random.rand(cEnd-cStart)
    for c in range(cStart,cEnd):
        cL,cx,cn = plex.computeCellGeometryFVM(c)
        ax.text(cx[0],cx[1],'%.2f' % U[c-cStart],ha='center',color='r')

    # compute and plot gradients of scalar at cell interfaces
    flux = ApproximateFluxTwoPoint(plex,U)
    for e in range(eStart,eEnd):
        if plex.getSupport(e).shape[0] == 1: continue
        eL,ex,en = plex.computeCellGeometryFVM(e)
        ax.text(ex[0],ex[1],'[%.1e,\n%.1e]' % (flux[e-eStart,0],flux[e-eStart,1]),ha='center',color='b',size=8)
    
    plt.show()
