from __future__ import division
from mpl_toolkits.axes_grid1 import make_axes_locatable
import numpy as np
import pylab as plt

"""

The following implements an analytic 2D solution to Richard's
equation for specified head through the side boundaries as given in
Table 1 of:

Tracy, F.T., Clean two- and three-dimensional analytical solutions of
Richards' equation for testing numerical solvers. Water Resources
Research, Vol. 42, 2006

"""

alpha   =  0.164  # [1/m]
hr      = -15.24  # [m]
L       =  15.24  # [m]
a       =  15.24  # [m]
theta_s =  1.0    # [-]
theta_r =  0.1    # [-]
k_s     =  1e-4   # [m/s]
ntimes  =  5

def S(h):
    return theta_r+(theta_s-theta_r)*np.exp(alpha*h)

# domain
x = np.linspace(0,a,100)
z = np.linspace(0,L,100)
X,Z = np.meshgrid(x,z)

# steady state solution
hbar0  = 1-np.exp(alpha*hr)
TopBC  = 1/alpha*np.log(np.exp(alpha*hr)+hbar0*np.sin(np.pi*x/a))
Beta   = np.sqrt(0.25*alpha**2+(np.pi/a)**2)
hbarss = hbar0*np.sin(np.pi*X/a)*np.exp(alpha/2*(L-Z))*np.sinh(Beta*Z)/np.sinh(Beta*L)
hss    = 1/alpha*np.log(np.exp(alpha*hr)+hbarss)
hss    = hss

# plot
fig,ax = plt.subplots(figsize=(26,5),nrows=2,ncols=ntimes+1,tight_layout=True)
cb = ax[0,-1].contourf(X,Z,S(hss),levels=np.linspace(theta_r,theta_s,10))
div = make_axes_locatable(ax[0,-1])
fig.colorbar(cb,cax=div.append_axes("right", size="5%", pad=0.05))
ax[0,-1].set_title("steady state")

# transient solution
def sol(t):
    converged = False
    k         = 1
    series    = 0
    while not converged:
        lamda   = k*np.pi/L
        c       = alpha*(theta_s-theta_r)/k_s
        gamma   = (Beta**2+lamda**2)/c
        term    = (-1)**k *lamda/gamma*np.sin(lamda*Z)*np.exp(-gamma*t)
        series += term
        k      += 1
        if (np.abs(term)).max() < 1e-25: converged = True
    phibar = 2*hbar0/(L*c)*np.sin(np.pi*X/a)*np.exp(alpha/2*(L-Z))*series
    hbar   = phibar + hbarss
    h      = 1/alpha*np.log(np.exp(alpha*hr)+hbar)
    return S(h)

# more plots
ts = 10.0**(1+np.arange(ntimes))
for i in range(ts.shape[0]):
    cb = ax[0,i].contourf(X,Z,sol(ts[i]),levels=np.linspace(theta_r,theta_s,10))
    div = make_axes_locatable(ax[0,i])
    fig.colorbar(cb,cax=div.append_axes("right", size="5%", pad=0.05))
    ax[0,i].set_title("%.1e seconds" % ts[i])

import h5py as h5
f = h5.File("pflotran.h5","r")
X = f["Coordinates"]["X [m]"][...]
Z = f["Coordinates"]["Z [m]"][...]

for key in f.keys():
    if "Time" not in key: continue
    t = float(key.split()[1])
    if t < 10: continue
    i = int(np.log10(t))-1
    S = f[key]["Liquid_Saturation"][:,0,:]

    cb = ax[1,i].pcolormesh(X,Z,S.T,vmin=theta_r,vmax=theta_s)
    div = make_axes_locatable(ax[1,i])
    fig.colorbar(cb,cax=div.append_axes("right", size="5%", pad=0.05))
    ax[1,i].set_title("%.1e seconds" % t)
    ax[1,i].set_xlim(0,15.24)
    ax[1,i].set_ylim(0,15.24)
plt.show()
