from __future__ import division
import sys, petsc4py
petsc4py.init(sys.argv)
from petsc4py import PETSc
import numpy as np
import pylab as plt
import hydrology as hyd
import fv as fv
from plot import PlotPlex,PlotFlux

saturation    = hyd.VanGenuchten
relative_perm = hyd.RelativePermeabilityMualem
flux_approx   = fv.ApproximateFluxTwoPoint

class RichardsFVM():
    
    def __init__(self,dim=2):
        self.dim   = dim
        self.n     = 2            # [-]      VanGenuchten parameters
        self.m     = 1-1/self.n   # [-]      VanGenuchten parameters (compatible with Mualem)
        self.alpha = 0.005        # [1/Pa]   VanGenuchten parameters
        self.Sr    = 0.1          # [-]      residual (minimum possible) saturation
        self.Ss    = 0.5          # [-]      maximum possible saturation
        self.rho   = 1000         # [kg/m^3] density of water
        self.Pr    = 101325       # [Pa]     reference pressure (atmospheric)
        self.mu    = 9.94e-4      # [Pa s]   viscosity of water
        self.Ki    = 9.1e-9       # [m^2]    intrinsic permeability
        self.pore  = 0.5          # [-]      porosity
        self.g     = np.zeros(self.dim)
        self.g[-1] = -9.81        # [m/s^2]  gravity
        self.plex  = None
        self.sec   = None

    def __call__(self,ts,step,t,Pl):
        fig,ax = plt.subplots(figsize=(7,7),tight_layout=True)
        ax.scatter(self.xc[:,0],self.xc[:,1],s=100,c=Pl,cmap=plt.cm.rainbow)
        plt.show()
                
    def createMesh(self,fname=None):
        if fname is not None:
            self.plex = PETSc.DMPlex().createExodusFromFile(fname,comm=PETSc.COMM_WORLD)
        else:
            coords = np.asarray([[0.0,0.0],
                                 [0.5,0.0],
                                 [1.0,0.0],
                                 [0.0,0.5],
                                 [0.5,0.5],
                                 [1.0,0.5],
                                 [0.0,1.0],
                                 [0.5,1.0],
                                 [1.0,1.0]])
            cells = np.asarray([[0,1,4,3],
                                [1,2,5,4],
                                [3,4,7,6],
                                [4,5,8,7]],dtype='int32')
            self.plex = PETSc.DMPlex().createFromCellList(self.dim,cells,coords,comm=PETSc.COMM_WORLD)
        if self.plex.getComm().getSize() > 1: self.plex.distribute(overlap=1)
        return self
    
    def markBoundary(self,TestForTop=None):
        plex = self.plex
        pStart,pEnd = plex.getChart()
        plex.createLabel("boundary")
        if TestForTop: plex.createLabel("top")
        for i in range(pStart,pEnd):
            if plex.getSupportSize(i) != 1: continue 
            if plex.getLabelValue("depth",i) == 1: 
                plex.setLabelValue("boundary",i,1)
                if TestForTop:
                    L,x,n = plex.computeCellGeometryFVM(i)
                    if TestForTop(L,x,n): plex.setLabelValue("top",i,1)
            
    def createSection(self):
        """
        create a section with 1 dof for each cell (pressure) and
        1 dof for each top boundary (surface water height)
        """
        pStart,pEnd = self.plex.getChart()
        cStart,cEnd = self.plex.getHeightStratum(0)
        self.sec = PETSc.Section().create(self.plex.getComm())
        self.sec.setChart(pStart,pEnd)
        for c in range(cStart,cEnd): self.sec.setDof(c,1)
        for p in range(pStart,pEnd):
            if self.plex.getLabelValue("top",p) == 1: self.sec.setDof(p,1)
        self.sec.setUp()
        self.plex.setDefaultSection(self.sec)

    def computeMeshInfo(self):
        cStart,cEnd = self.plex.getHeightStratum(0)
        self.xc = np.zeros((cEnd-cStart,self.dim)) # cell centroids
        self.vc = np.zeros(cEnd-cStart)            # cell volumes
        for c in range(cStart,cEnd):
            self.vc[c-cStart],self.xc[c-cStart,:],dummy = self.plex.computeCellGeometryFVM(c)

    def computeState(self,Pl):
        Se,dSe = saturation(self.n,self.m,self.alpha,Pc=self.Pr-Pl,derivative=True)
        S      = Se*(self.Ss-self.Sr)+self.Sr
        Kr     = relative_perm(Se,self.n)
        Psi    = Pl-self.rho*np.dot(self.xc,self.g)
        dS_dPl = (self.Ss-self.Sr)*dSe*(-1)
        return Se,S,dS_dPl,Kr,Psi

    def computeFlux(self,x,n,cL,cR,vL,vR):
        """
        x  = centroid of the face
        n  = normal of the face pointing from cL to cR
        cL = point number of the left cell 
        cR = point number of the right cell 
        vL = value of the left cell
        vR = value of the right cell
        """
        flux = (vR-vL)/np.linalg.norm(self.xc[cR,:]-self.xc[cL,:])
        return flux
    
    def computeRHS(self,ts,t,Pl,RHS):
        """
        Put Richard's equation into the form: udot = F(u)
        
        d/dt(pore*rho*S) = div(Ki*Kr/mu*grad(Pl-rho*g*z))

        after applying the chain rule
        
        pore*rho*dS/dSe*dSe/dPc*dPc/dPl*dPl/dt = div(Ki*Kr/mu*grad(Pl-rho*g*z))

        or
        
        dPl/dt = 1/(pore*rho*dS/dSe*dSe/dPc*dPc/dPl) * div(Ki*Kr/mu*grad(Pl-rho*g*z))
        """
        Se,S,dS_dPl,Kr,Psi = self.computeState(Pl[...])
        eStart,eEnd = self.plex.getHeightStratum(1)
        for e in range(eStart,eEnd):
            v,x,n = self.plex.computeCellGeometryFVM(e)
            supp = self.plex.getSupport(e)
            if supp.shape[0] == 1: continue # handle boundary
            flux = self.computeFlux(x,n,supp[0],supp[1],Psi[supp[0]],Psi[supp[1]])
            flux *= self.Ki*Kr[supp[flux>0]]/self.mu # upwind
            RHS[...][supp[0]] += flux/self.vc[supp[0]]
            RHS[...][supp[1]] -= flux/self.vc[supp[1]]

def Top(L,x,n):
    if abs(x[1]-1.0)<1e-12: return True
    return False

#try:
#    fv = RichardsFVM().createMesh(sys.argv[1])
#except:
fv = RichardsFVM().createMesh()
fv.computeMeshInfo()
fv.markBoundary(TestForTop=Top)
fv.createSection()

# initally constant pressure
Pl = fv.plex.createGlobalVec()
Pl.view()
#Pl[...] = 100325.0 

#ts  = PETSc.TS().create()
#ts.setType(PETSc.TS.Type.EULER)
#RHS = fv.plex.createGlobalVec()
#ts.setRHSFunction(fv.computeRHS,RHS)
#ts.setMonitor(fv)
#ts.setFromOptions()

#ts.solve(Pl)
