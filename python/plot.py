import pylab as plt
import numpy as np

def PlotPlex(plex,ax,clr='k'):
    dim = plex.getDimension()
    assert dim == 2
    eStart,eEnd = plex.getHeightStratum(1)
    for e in range(eStart,eEnd):
        eL,ex,en = plex.computeCellGeometryFVM(e)
        en = en[::-1]; en[0] *= -1
        x = np.zeros((2,dim))
        x[0,:] = ex+0.5*eL*en
        x[1,:] = ex-0.5*eL*en
        ax.plot(x[:,0],x[:,1],'-',color=clr,alpha=0.5,lw=2)

def PlotFlux(plex,flux,ax):
    dim = plex.getDimension()
    eStart,eEnd = plex.getHeightStratum(1)
    assert dim == 2
    assert (eEnd-eStart)==flux.shape[0]
    X = np.zeros((eEnd-eStart,dim))
    for e in range(eStart,eEnd):
        dummy,ex,dummy = plex.computeCellGeometryFVM(e)
        X[e-eStart,:] = ex
    clr = np.apply_along_axis(np.linalg.norm,1,flux)
    ax.quiver(X[:,0],X[:,1],flux[:,0],flux[:,1],clr,pivot='mid')
