PflotranNext
============

This repository is a place to store progress in rethinking Pflotran to
utilize more of the new structures that PETSc has added. Specifically
I am trying to understand how to use DMPlex to create more of a
framework to solve PDEs using different finite volume methods. I call
this PflotranNext simply such that if the code proves useful, we can
use it on projects by calling it Pflotran. You will need the latest
tip of the master branch of PETSc. I configure PETSc with the
following line

  ./configure --download-chaco --download-ctetgen --download-exodusii --download-netcdf --download-triangle --download-hdf5

You will also need to install petsc4py. 

Plan
----

* Methods: two point flux, least squares, mimetic finite differences, Wheeler-Yotov
* Physics: Richards, Richards+thermal, DSW, DSW+thermal, all coupled
* Implicit/explicit time stepping
* Coupling strategies: sequential, iterative, implicit
* Physics, methods, time steppers, and coupling should all be a commandline choice
* Benchmarks: Create a suite of benchmark problems as I go

Why?
----

* Side project to help me learn new technologies / hydrology / climate
* Address the usefulness of new methods, in the pursuit of scalable methods
* Develop and verify as I go, this does not seem to be done in most codes

Goal
----

* Get Richard's working in parallel for 2pt flux, implicit, verify

